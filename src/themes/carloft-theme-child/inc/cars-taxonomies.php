<?php 

// Create Category Taxonomy for Cars Custom Post Type
add_action( 'init', 'create_cars_category_taxonomy', 0 );
function create_cars_category_taxonomy() {
 
  $labels = array(
    'name' => _x( 'Categories', 'taxonomy general name' ),
    'singular_name' => _x( 'Category', 'taxonomy singular name' ),
    'search_items' =>  __( 'Search Categories' ),
    'all_items' => __( 'All Categories' ),
    'parent_item' => __( 'Parent Category' ),
    'parent_item_colon' => __( 'Parent Category:' ),
    'edit_item' => __( 'Edit Category' ), 
    'update_item' => __( 'Update Category' ),
    'add_new_item' => __( 'Add New Category' ),
    'new_item_name' => __( 'New Category Name' ),
    'menu_name' => __( 'Categories' ),
  ); 	
 
  register_taxonomy('category',array('inventory'), array(
    'hierarchical' => true,
    'labels' => $labels,
    'show_ui' => true,
    'show_admin_column' => true,
    'query_var' => true,
    'rewrite' => array( 'slug' => 'type' ),
  ));
}

// Create Brand Taxonomy for Cars Custom Post Type
add_action( 'init', 'create_cars_brands_taxonomy', 0 );
function create_cars_brands_taxonomy() {
 
  $labels = array(
    'name' => _x( 'Brands', 'taxonomy general name' ),
    'singular_name' => _x( 'Brand', 'taxonomy singular name' ),
    'search_items' =>  __( 'Search Brands' ),
    'all_items' => __( 'All Brands' ),
    'parent_item' => __( 'Parent Brand' ),
    'parent_item_colon' => __( 'Parent Brand:' ),
    'edit_item' => __( 'Edit Brand' ), 
    'update_item' => __( 'Update Brand' ),
    'add_new_item' => __( 'Add New Brand' ),
    'new_item_name' => __( 'New Brand Name' ),
    'menu_name' => __( 'Brands' ),
  ); 	
 
  register_taxonomy('brands',array('inventory'), array(
    'hierarchical' => false,
    'labels' => $labels,
    'show_ui' => true,
    'show_admin_column' => true,
    'query_var' => true,
    'rewrite' => array( 'slug' => 'type' ),
  ));
}

// Create Color Taxonomy for Cars Custom Post Type
add_action( 'init', 'create_cars_colors_taxonomy', 0 );
function create_cars_colors_taxonomy() {
 
  $labels = array(
    'name' => _x( 'Colors', 'taxonomy general name' ),
    'singular_name' => _x( 'Color', 'taxonomy singular name' ),
    'search_items' =>  __( 'Search Colors' ),
    'all_items' => __( 'All Colors' ),
    'parent_item' => __( 'Parent Color' ),
    'parent_item_colon' => __( 'Parent Color:' ),
    'edit_item' => __( 'Edit Color' ), 
    'update_item' => __( 'Update Color' ),
    'add_new_item' => __( 'Add New Color' ),
    'new_item_name' => __( 'New Color Name' ),
    'menu_name' => __( 'Colors' ),
  ); 	
 
  register_taxonomy('colors',array('inventory'), array(
    'hierarchical' => false,
    'labels' => $labels,
    'show_ui' => true,
    'show_admin_column' => true,
    'query_var' => true,
    'rewrite' => array( 'slug' => 'type' ),
  ));
}


?>