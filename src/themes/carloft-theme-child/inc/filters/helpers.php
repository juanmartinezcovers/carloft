<?php 

/*
* Return an Array of dealer_ids in meta field structure
*/
function get_meta_dealer_ids(){

    $dealer_ids = get_field('dealer_ids', 'options');

    if(empty($dealer_ids))
        return [];

    $result = [];
    foreach ($dealer_ids as $value) {
        $meta_field = array(
            'key' => 'dealerid',
            'value' => $value['dealer_id'],
            'compare' => '='
        );

        $result[] = $meta_field;
    }

    return $result;

}


?>