<?php 
require_once 'helpers.php';
function get_inventory_img($post, $size='large'){
    $img = get_the_post_thumbnail_url($post->ID, 'large');
    if(empty($img)){
        $img = get_stylesheet_directory_uri() . '/dist/images/filters/default-img.jpg';
    }
    return $img;
}
function get_meta_fields($post){
    $fields = array(
        'mileage' => null,
        'bwpayment' => null,
        'make' => null,
        'type' => null,
        'vin'  => null
    );
    foreach ($fields as $key => $value) {
        $meta_val = get_post_meta($post->ID, $key, true);
        if ($key == 'mileage') {
            $meta_val = number_format( intval($meta_val) );
        } 
        $fields[$key] = $meta_val;
    }
    return $fields;
}
function form_data_to_meta_query($data, $extra_metas=[]) {
    if (empty($data) && empty($extra_metas))
        return [];
    $meta_query = array('relation' => "AND");
    $form_data_meta_query = [];
    $extra_meta_query = [];    
    if (!empty($data)) {
        $form_data_meta_query = array('relation' => "AND");
        $type_meta_query = array('relation' => "OR");
        foreach ($data as $input_value) {
            $meta_field = array(
                'key' => $input_value["name"],
                'value' => $input_value["value"],
                'compare' => '='
            );
            if ($input_value["name"] == 'type') {
                $type_meta_query[] = $meta_field;
            
            } else {
                $form_data_meta_query[] = $meta_field;
            }
            
        }
        if (count($form_data_meta_query) > 1) {
            $meta_query[] = $form_data_meta_query;    
        }
        
        if (count($type_meta_query) > 1){
            $meta_query[] = $type_meta_query;
        }
    }
    if ( !empty( $extra_metas ) ) {
        $extra_meta_query = array('relation' => "OR");
        foreach ($extra_metas as $meta) {
            $extra_meta_query[] = $meta;
        }
        $meta_query[] = $extra_meta_query;
    }
    
    return $meta_query;
}
function inventory_query($meta_query, $s, $paged=1){
    $items = [];
    $args = [
        'post_type' => 'inventory',
        'post_status' => 'publish',
        'posts_per_page' => -1,
        // 'posts_per_page' => 6,
        // 'paged' => $paged,
        'meta_query' => $meta_query,
        //'s' => $s
    ];
    $query = new WP_Query( $args );
    while ( $query->have_posts() ) { 
        $query->the_post();
        $post = get_post();
        $meta_fields = get_meta_fields($post);
        $title = get_the_title($post);
        $image = get_inventory_img($post, 'large');
        $items[] = array(
            'id' => $post->ID,
            'meta_fields' => $meta_fields,
            'title' => $title,
            'image' => $image
        );
    }
    
    return [
        'items' => $items, 
        'max_num_pages' => $query->max_num_pages
    ];
    wp_reset_postdata();
}
add_action('wp_ajax_nopriv_filter_inventory', 'filter_inventory');
add_action('wp_ajax_filter_inventory', 'filter_inventory');
function filter_inventory(){
    // Check parameters
    $form_data  = isset( $_POST['form_data'] ) ? $_POST['form_data'] : false;
    $paged  = isset( $_POST['paged'] ) ? intval($_POST['paged']) : 1;
    $s  = isset( $_POST['s'] ) ? $_POST['s'][0]['value'] : '';
    if( !$form_data and !$s and $paged <= 1 ){
        $response = array(
            'message' => __('No Data received', 'wpduf'),
            'data' => null
        );
        wp_send_json( $response );
    } else {
        
        // Extra arguments
        $dealer_ids = get_meta_dealer_ids();
        
        
        if(!empty($s)) {
            $stock = array(
                "name" => array("stocknumber", "make"),
                "value" => $s
            );
            $form_data[] = $stock;
        }
        
        $meta_query = form_data_to_meta_query($form_data, $dealer_ids);
        $query = inventory_query($meta_query, $s, $paged);
        $response = array(
            'message' => __('Success', 'wpduf'),
            'data' => $query['items'],
            'max_num_pages' => $query['max_num_pages']
        );
        wp_send_json( $response );
    }
}
