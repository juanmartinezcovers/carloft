<?php 

function step_form_submit() {

    $form  = isset( $_POST['form'] ) ? $_POST['form'] : false;
    $steps = isset( $_POST['steps'] ) ? $_POST['steps'] : false;

    function printArray($array = null){
        if ($array == null){
            return;
        }

        if(is_array($array)){
            ob_start();
            foreach( $array as $index => $inner ){
                $time = 'years';
                if ($index != 0)
                    $time = 'months';

                
                ?>
                <span><?php echo $inner . ' ' . $time . ' ';?></span>
                <?php 
            }
            $content = ob_get_contents();
            ob_end_clean();
            return $content;
        } else {
            return $array;
        }

    }

    ob_start();
    ?>  
    <div>
        You have a new approval request on your site: <br>

        From: 

        <?php foreach( $form as $data => $value ): ?>
            <p><?php echo $data . ': ' . $value ?></p>
        <?php endforeach;?> 

        Details: <br>

        <?php foreach( $steps as $data => $value ):?>
        <p><?php echo $data . ': ' .  printArray($value) ;?></p>
        <?php endforeach; ?>
    </div>
    <?php
    $html = ob_get_contents();
    ob_end_clean();
    $receiver = get_field( 'email', 'option' );
    $headers = array('Content-Type: text/html; charset=UTF-8','From: Carloft <adfmail@carloft.ca>');

    wp_mail( $receiver, 'New submit to sell a car', $html , $headers );
    wp_send_json( 'true' );
  
}
add_action( 'wp_ajax_nopriv_send-mail-approved', 'step_form_submit' );
add_action( 'wp_ajax_send-mail-approved', 'step_form_submit' );
    