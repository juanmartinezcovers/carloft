<?php 

function modal_form_submit() {

    $form  = isset( $_POST['form'] ) ? $_POST['form'] : false;
    
    $maker = $form['make'];
    $model = $form['model'];
    $year = $form['year'];
    $message = $form['message'];
    $seller = $form['seller'];
    $phone = $form['phone'];
    $email = $form['email'];
    $km = $form['km'];
    $used="used";

    if($km==0 || $km=="0"){ $used='new'; }
    $seller_=explode(" ",$seller);
    $name_part_1=$seller;
    $name_part_2="(NOT LAST NAME)";

    $total=count($seller_);
    switch($total)
    {
      case 1:
        $name_part_1=$seller_[0];
      break;
      case 2:
        $name_part_1=$seller_[0];
        $name_part_2=$seller_[1];        
      break;   
      default:
        if($total>2)
        {
          $name_part_1=$seller_[0];
          $name_part_1.=" ".$seller_[1];  

          $name_part_1=$seller_[0]+" "+$seller_[1];
          $name_part_2="";
          $space="";
          foreach($seller_ as $id_=>$name)
          {
            if($id_>=2)
            {
              $name_part_2.=$space.$name;
              $space="";
            }            
          }          
        }
      break;           
    }
$html='
<?xml version="1.0"?>
<?adf version="1.0"?>
<adf>
	<prospect status="new">
		<id source="Test Lead">'.date("YmdHmsU").'</id>
		<requestdate>'.date("Y-m-d").'T'.date("H:i:s").'+00:00</requestdate>
		<vehicle status="'.$used.'" interest="buy">
			<year>'.$year.'</year>
			<make>'.$maker.'</make>
			<model>'.$model.'</model>
			<vin>TEST</vin>
			<stock>TEST</stock>
			<trim>TEST</trim>		
		</vehicle>
		<customer>
			<contact>
				<name part="first">'.$name_part_1.'</name>
				<name part="last">'.$name_part_2.'</name>
				<email>'.$email.'</email>
				<phone>'.$phone.'</phone>
				<address>
					<city>Cornwall North</city>
					<regioncode>ON</regioncode>
					<postalcode>K6K 1R8</postalcode>
		  		</address>					
			</contact>		
			<comments>'.$message.'</comments>
		</customer>
		<vendor>
			<vendorname>Car Loft</vendorname>
		</vendor>
		<provider>
			<name>Roadster Online</name>
			<service>Test Drive</service>
			<url>https://dealers.roadster.com/carloft/customers/3053689</url>
		</provider>		
	</prospect>
</adf>';


    $receiver = get_field( 'email', 'option' );
    $headers = array('Content-Type: text/html; charset=UTF-8','From: Carloft <adfmail@carloft.ca');

    wp_mail( $receiver, 'New submit to sell a car', $html , $headers );
    wp_send_json( 'true' );
  
}
add_action( 'wp_ajax_nopriv_send-mail', 'modal_form_submit' );
add_action( 'wp_ajax_send-mail', 'modal_form_submit' );
    