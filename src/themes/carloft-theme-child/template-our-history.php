<?php  
	 /* Template Name: Our History */
	get_header();
?> 

<section id="welcome-carloft" class="welcome-carloft">
	<?php get_template_part('template-parts/history/welcome', 'carloft'); ?>
</section>

<!-- 
<section class="our-clients" style="background-color:<?php the_field('background_our_clients');?>">
	<?php // get_template_part('template-parts/history/our', 'clients'); ?>
</section> -->


<section class="banner-promotion-desktop">
	<?php get_template_part('template-parts/home/banner', 'promotion-our-story'); ?>
</section>	


<section class="our-donations">
	<?php get_template_part('template-parts/history/our', 'donations'); ?>
</section>	



<? get_footer(); ?>