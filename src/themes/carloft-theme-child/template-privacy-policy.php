<?php  
	 /* Template Name: Privacy Policy */
	get_header();
?> 

<section class="privacy-policy">
	<div class="container">
		<p><?php the_field('content_privacy_and_policy'); ?></p>
	</div>
</section>

<?php get_footer(); ?>