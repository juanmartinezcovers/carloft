<?php

  function update_jquery(){
    $wp_admin = is_admin();
    $wp_customizer = is_customize_preview();

    if ( $wp_admin || $wp_customizer ){
      //Ups, we are on the admin or customizer, don't deregister scripts here;
      return;
    }else {
      wp_deregister_script( 'jquery' );
      wp_deregister_script( 'jquery-core' );
      wp_register_script( 'jquery-core', get_stylesheet_directory_uri() . '/dist/js/jquery.min.js', array(), '3.5.1' );
      wp_deregister_script( 'jquery-migrate' );
      wp_register_script( 'jquery-migrate', get_stylesheet_directory_uri() . '/dist/js/jquery-migrate.min.js', array(), '3.3.1' );

      wp_register_script( 'jquery', false, array( 'jquery-core' ), null, false );
      wp_enqueue_script( 'jquery' );
    }
  }
  add_action( 'wp_enqueue_scripts', 'update_jquery' );

  /*
  function wpmailsmtp_enable_autotls( $mailer ) {
    $mailer->SMTPAutoTLS = true;
    return $mailer;
  }
  add_filter( 'wp_mail_smtp_custom_options', 'wpmailsmtp_enable_autotls' );
  
  function setup_phpmailer_init( $phpmailer )
  {
    $phpmailer->Host = 'smtp.mandrillapp.com'; // for example, smtp.mailtrap.io
    $phpmailer->Port = 587; // set the appropriate port: 465, 2525, etc.
    $phpmailer->Username = 'Talentbox.la'; // your SMTP username
    $phpmailer->Password = 'Yvolku1cZHS7p9wsDfgZZA'; // your SMTP password
    $phpmailer->SMTPAuth = true; 
    $phpmailer->SMTPSecure = 'tls'; // preferable but optional [ssl]
    $phpmailer->IsSMTP();
  }
  add_action( 'phpmailer_init', 'setup_phpmailer_init' );*/

  function enqueue_load() {
    //Font Awesome
    wp_enqueue_style( 'load-fa', 'https://use.fontawesome.com/releases/v5.3.1/css/all.css' );
    //Parent Style
    wp_enqueue_style( 'parent-style', get_template_directory_uri().'/style.css' );
    //Roboto Font
    wp_enqueue_style( 'roboto', 'https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700;900&display=swap');
    //Oswald Font
    wp_enqueue_style( 'oswald', 'https://fonts.googleapis.com/css2?family=Oswald:wght@300;400;500;700&display=swap');
    //Bootstrap
    wp_enqueue_style( 'bootstrap_css', get_stylesheet_directory_uri() . '/dist/css/bootstrap.min.css', array(), '4.1.3'); 
    //Dom Purify
    wp_enqueue_script('dom-purify-js', get_stylesheet_directory_uri() . '/dist/js/domPurify.min.js', array());
    //Bootstrap JS
    wp_enqueue_script( 'bootstrap_js', get_stylesheet_directory_uri() . '/dist/js/bootstrap.min.js', array('jquery'), '4.1.3', true); 
    //Child Theme
    wp_enqueue_style( 'style-css', get_stylesheet_directory_uri() . '/dist/css/style.css' );
    //Cloud 9 Slider
    wp_enqueue_script( 'cloud9-slider', get_stylesheet_directory_uri() . '/dist/js/cloud9-slider.js', array('jquery') );
    //Slick CSS
    wp_enqueue_style( 'slick-css', get_stylesheet_directory_uri() . '/dist/css/slick.min.css');
    //Slick JS
    wp_enqueue_script( 'slick', get_stylesheet_directory_uri() . '/dist/js/slick.min.js', array('jquery') );
    //Slider Component
    wp_enqueue_script( 'cta-slider', get_stylesheet_directory_uri() . '/dist/js/slider-cta.js', array('jquery') );
    //Global JS
    wp_enqueue_script( 'global', get_stylesheet_directory_uri() . '/dist/js/global.js', array('jquery'));
    //Map JS
    wp_enqueue_script( 'map', get_stylesheet_directory_uri() . '/dist/js/map.js', array('jquery'));
    // Contact JS
    wp_enqueue_script( 'contact', get_stylesheet_directory_uri() . '/dist/js/contact.js', array('jquery') );
    // Our Story js
    wp_enqueue_script( 'our-story', get_stylesheet_directory_uri() . '/dist/js/our-story.js', array('jquery') ); 
    //Step by Step Modal
    wp_enqueue_script( 'step-by-step', get_stylesheet_directory_uri() . '/dist/js/step-by-step.js', array('jquery') );
    wp_localize_script( 'step-by-step', 'ajax_var', array(
      'url'    => admin_url( 'admin-ajax.php' ),
      'action' => 'send-mail'
  ));
    //Step by Step Page
    if (is_page('how-it-works')){
      wp_enqueue_script( 'step-by-step-page', get_stylesheet_directory_uri() . '/dist/js/steps-page.js', array('jquery') );
      wp_localize_script( 'step-by-step-page', 'ajax_var', array(
        'url'    => admin_url( 'admin-ajax.php' ),
        'action' => 'send-mail-approved'
      ));
    }

    if (is_page_template('template-inventory.php')){
      wp_enqueue_script( 'inventory', get_stylesheet_directory_uri() . '/dist/js/inventory.js', array('jquery') );

      wp_localize_script( 'inventory', 'ajax_var', array(
        'url'    => admin_url( 'admin-ajax.php' ),
        'nonce'  => wp_create_nonce( 'my-ajax-nonce' ),
        'action' => 'event-list'
      ) );

    }

    // Google Maps
    if ( is_page('contact') ){
      wp_enqueue_script( 'google-map', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyCB-_Ks8qKJ0NK-7yqqFU1_MgzyZ8OO52I&callback=initMap', array(), null, true );
    }

    //Express Integration JS
    wp_enqueue_script( 'express-analytics', get_stylesheet_directory_uri() . '/dist/js/express-analytics.js', array(), null, true );
    wp_enqueue_script( 'express-checkout-frame', get_stylesheet_directory_uri() . '/dist/js/express-checkout-frame.js', array(), null, true );


  }
  add_action( 'wp_enqueue_scripts', 'enqueue_load' );

  //Options Page ACF Pro
  if( function_exists('acf_add_options_page') ) {

      acf_add_options_page();
      
  }

/*
* Creating a function to create our CPT
*/
function custom_post_type() {
  // Set UI labels for Custom Post Type
      $labels = array(
          'name'                => _x( 'Contact Page', 'Post Type General Name', 'twentytwenty' ),
          'singular_name'       => _x( 'contact page', 'Post Type Singular Name', 'twentytwenty' ),
          'menu_name'           => __( 'Contact Pages', 'twentytwenty' ),
          'parent_item_colon'   => __( 'Parent contact-form', 'twentytwenty' ),
          'all_items'           => __( 'All Contact Pages', 'twentytwenty' ),
          'view_item'           => __( 'View contact page', 'twentytwenty' ),
          'add_new_item'        => __( 'Add New Contact Page', 'twentytwenty' ),
          'add_new'             => __( 'Add New', 'twentytwenty' ),
          'edit_item'           => __( 'Edit Contact Page', 'twentytwenty' ),
          'update_item'         => __( 'Update Contact Page', 'twentytwenty' ),
          'search_items'        => __( 'Search Contact Page', 'twentytwenty' ),
          'not_found'           => __( 'Not Found', 'twentytwenty' ),
          'not_found_in_trash'  => __( 'Not found in Trash', 'twentytwenty' ),
      );
  // Set other options for Custom Post Type
      $args = array(
          'label'               => __( 'contact page', 'twentytwenty' ),
          'description'         => __( 'contact pages', 'twentytwenty' ),
          'labels'              => $labels,
          // Features this CPT supports in Post Editor
          'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields', ),
          // You can associate this CPT with a taxonomy or custom taxonomy. 
          'taxonomies'          => array( 'inventory' ),
          /* A hierarchical CPT is like Pages and can have
          * Parent and child items. A non-hierarchical CPT
          * is like Posts.
          */ 
          'hierarchical'        => false,
          'public'              => true,
          'show_ui'             => true,
          'show_in_menu'        => true,
          'show_in_nav_menus'   => true,
          'show_in_admin_bar'   => true,
          'menu_position'       => 5,
          'can_export'          => true,
          'has_archive'         => true,
          'exclude_from_search' => false,
          'publicly_queryable'  => true,
          'capability_type'     => 'post',
          'show_in_rest' => true,
          'menu_icon'           => 'dashicons-media-text'
      );
      // Registering your Custom Post Type
      register_post_type( 'contact-form', $args );
  }
  /* Hook into the 'init' action so that the function
  * Containing our post type registration is not 
  * unnecessarily executed. 
  */
  add_action( 'init', 'custom_post_type', 0 );
/*
* Creating a function to create our CPT
*/
function custom_post_type_car() {
  // Set UI labels for Custom Post Type
      $labels = array(
          'name'                => _x( 'Cars', 'Post Type General Name', 'twentytwenty' ),
          'singular_name'       => _x( 'cars page', 'Post Type Singular Name', 'twentytwenty' ),
          'menu_name'           => __( 'Cars', 'twentytwenty' ),
          'parent_item_colon'   => __( 'Parent cars', 'twentytwenty' ),
          'all_items'           => __( 'All Cars Pages', 'twentytwenty' ),
          'view_item'           => __( 'View cars', 'twentytwenty' ),
          'add_new_item'        => __( 'Add New Cars', 'twentytwenty' ),
          'add_new'             => __( 'Add New', 'twentytwenty' ),
          'edit_item'           => __( 'Edit Cars', 'twentytwenty' ),
          'update_item'         => __( 'Update Cars', 'twentytwenty' ),
          'search_items'        => __( 'Search Cars', 'twentytwenty' ),
          'not_found'           => __( 'Not Found', 'twentytwenty' ),
          'not_found_in_trash'  => __( 'Not found in Trash', 'twentytwenty' ),
      );
  // Set other options for Custom Post Type
      $args = array(
          'label'               => __( 'cars', 'twentytwenty' ),
          'description'         => __( 'cars', 'twentytwenty' ),
          'labels'              => $labels,
          // Features this CPT supports in Post Editor
          'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields', ),
          // You can associate this CPT with a taxonomy or custom taxonomy. 
          'taxonomies'          => array( 'inventory' ),
          /* A hierarchical CPT is like Pages and can have
          * Parent and child items. A non-hierarchical CPT
          * is like Posts.
          */ 
          'hierarchical'        => false,
          'public'              => true,
          'show_ui'             => true,
          'show_in_menu'        => true,
          'show_in_nav_menus'   => true,
          'show_in_admin_bar'   => true,
          'menu_position'       => 5,
          'can_export'          => true,
          'has_archive'         => true,
          'exclude_from_search' => false,
          'publicly_queryable'  => true,
          'capability_type'     => 'post',
          'show_in_rest' => true,
          'menu_icon'           => 'dashicons-car'
      );
      // Registering your Custom Post Type
      register_post_type( 'inventory', $args );
  }
  /* Hook into the 'init' action so that the function
  * Containing our post type registration is not 
  * unnecessarily executed. 
  */
  add_action( 'init', 'custom_post_type_car', 0 );

  // Add Options Page ACF 
  if( function_exists('acf_add_options_page') ) {
	
    acf_add_options_page();
    
  }

/**
 * This function modifies the main WordPress query to include an array of 
 * post types instead of the default 'post' post type.
 *
 * @param object $query The main WordPress query.
*/
function include_custom_post_types_in_search_results( $query ) {
  if ($query->is_search()) {
      $query->set( 'post_type', array( 'inventory') );
  }
  return $query;
}
add_action( 'pre_get_posts', 'include_custom_post_types_in_search_results' );

/**
 * Adding Google Analytics
 */

function google_analytics(){
  ob_start();
  ?>
    <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-49506774-2"></script>
  <script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
  gtag('config', 'UA-49506774-2');
  </script>
  <?php 
  $content = ob_get_contents();
  ob_end_clean();

  echo $content;
}
add_action('wp_head', 'google_analytics');

/*
* Taxonomies for Cars CPT
*/
require_once 'inc/cars-taxonomies.php';

/*
* Taxonomies for Cars CPT
*/
require_once 'inc/filters/filters-controller.php';

/**
 * Ajax Step by Step
 */
require_once 'inc/ajax/step-by-step.php';
/**
 * Ajax Step by Step
 */
require_once 'inc/ajax/step-page-submit.php';

add_filter('acf/settings/remove_wp_meta_box', '__return_false');