<?php  
/* Template Name: Inventory */
get_header();


$dealer_ids = get_meta_dealer_ids();
$meta_query = form_data_to_meta_query([], $dealer_ids);

$args = [
	'post_type' => 'inventory',
	'post_status' => 'publish',
	'posts_per_page' => -1,
	'meta_query' => $meta_query
	// 'paged' => get_query_var( 'paged' )
	//'paged'
	//'order' => 'DESC'
	// 'orderby' => 'date'
];

$query = new WP_Query( $args );

?> 


<div id="inventory" class="container py-4">
	<div class="row no-gutters">
		<div class="col-md-2 pr-2">
			<div>

				<!-- <div class="filters-title">
					<?php //get_template_part( 'icons/icon-filter'); ?>
					<h6>Filter</h6>
				</div> -->
				
				<form id="main-filter" method="POST">
					<ul class="filters">
						<li class="filter">
							<?php get_template_part('template-parts/filters/filter-category') ?>
						</li>
						<li class="filter">
						<?php get_template_part('template-parts/filters/filter-brands') ?>
						</li>
						<li class="filter">
						<?php get_template_part('template-parts/filters/filter-colors') ?>
						</li>
					</ul>
				</form>
				
			</div>
		</div>
		<div class="col-md-10">
			<form id="search">
				<div class="search">
					<div class="search-bar">
						<i class="fas fa-search"></i>
						<input class="form-control form-control-lg search-input" name="s" type="text" placeholder="Search" value="<?php echo $_POST['inventory_search']; ?>">
						<?php if( isset($_POST['inventory_search']) ):?>
							<div data-submit="true"></div>
						<?php endif; ?>
					</div>

					<script>
						jQuery(document).ready(function($){
							let formSearchInventory = $('#search');
							let dataSubmit = $('[data-submit="true"]');

							if ( dataSubmit.length >= 1 ){
								formSearchInventory.submit();
							}
						});
					</script>
					<div class="layout-buttons d-none">
						<button class="layout-button" type="button"><img src="<?php echo get_stylesheet_directory_uri() . '/dist/images/filters/layout-icon-shop.png'; ?>" alt="Shop View"></button>
						<button class="layout-button" type="button"><img src="<?php echo get_stylesheet_directory_uri() . '/dist/images/filters/layout-icon-detail.png'; ?>" alt="Detail View"></button>
					</div>
				</div>
			</form>

			<section class="items-container">
				<div class="row ml-md-2" id="items">

				<?php if ( $query->have_posts() ) :  ?>
					<?php while ( $query->have_posts() ) : $query->the_post(); ?>
					<?php $meta_fields = get_meta_fields($post); ?>
					
					<div class="col-md-6 px-md-4 pb-md-5 post-id-<?php echo $post->ID; ?> ">
						<figure>
							<a class="" href="https://express.carloft.ca/express/<?php echo $meta_fields['vin']; ?>"><img class="img-fluid inventory-img" src="<?php echo get_inventory_img($post, 'large'); ?>" alt="<?php the_title(); ?>"></a>
						</figure>
						<div class="item px-0 px-md-3">
							<div class="info">
								<p class="name">
									<?php the_title(); ?> <span>/ <?php echo $meta_fields['mileage'] ?> km</span>
								</p>
								<p class="price">$<?php echo $meta_fields['bwpayment'] ?></p>
								<p class="payment">Bi-Weekly</p>
							</div>
							<div class="mt-1">
								<a class="primary-button" href="https://express.carloft.ca/express/<?php echo $meta_fields['vin']; ?>">BUILD MY DEAL</a>
							</div>
						</div>
					</div>
					
					<?php endwhile; ?>
					<?php wp_reset_postdata(); ?>
					

				<?php else: ?>
					<h4>NO CARS WERE FOUND.</h4>
				<?php endif; ?>
					
				</div>
				<!-- <div class="load-more-container">
					<button id="load-more" data-paged="1">Load more</button>
				</div> -->
			</section>
			
			
		</div>
	</div>
</div>

<?php get_footer(); ?>
