<?php  
	 /* Template Name: Steps */
	get_header();
?> 

<section id="main-steps-aprobation" class="steps-approbation container py-5">
    <div class="progress-bar-container px-0 px-md-5">
        <div class="progress-bar">
            <div class="progress"></div>
        </div>
    </div>
    <form id="step-form-page">
    <div class="tabs">        
        <div class="tabs-container">
            <div class="tab">
            <?php get_template_part('template-parts/step-page/step', '1'); ?>
            </div>
            <div class="tab">
            <?php get_template_part('template-parts/step-page/step', '2'); ?>
            </div>
            <div class="tab">
            <?php get_template_part('template-parts/step-page/step', '3'); ?>
            </div>
            <div class="tab">
            <?php get_template_part('template-parts/step-page/step', '4'); ?>
            </div>
            <div class="tab">
            <?php get_template_part('template-parts/step-page/step', '5'); ?>
            </div>
            <div class="tab">
            <?php get_template_part('template-parts/step-page/step', '6'); ?>
            </div>
        </div>
        
        <div class="controls">
            <button type="button" data-prev="0" class="prev">Go Back</button>
            <button type="button" data-step="1" class="next">Continue</button>
        </div>
    </form>
    </div>
</section>

<?php get_template_part( 'template-parts/step-page/congrats', 'modal' ); ?> 
<?php get_footer();?>