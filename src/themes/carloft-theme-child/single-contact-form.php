<?php 

/**
 * Template for contact form cpt
 */

$location = get_the_title();
$id = get_the_ID();
get_header(); ?>

<div class="container-location-contact-us container py-4">
    <div class="row">

        <div class="col-md-3 location-details">
            <h2 class="location-name">
            <?php echo strtoupper($location); ?><br>
            <!-- <span class="green">LOCATION</span> -->
            </h2>
            <!-- <div class="star-review">
                <div class="d-flex align-items-center">
                    <small class="points">5/5</small>
                    <div class="stars ml-2">
                        php get_template_part('icons/icon', 'stars'); 
                    </div>
                </div>
                <div class="reviews-count mt-n1">
                    <small>(20 review)</small>
                </div>
            </div> -->
            <div class="geo-location d-flex mt-3">
                <div class="geo-icon mr-2">
                    <?php get_template_part( 'icons/icon', 'marker' ); ?>
                </div>
                <small class="address"><?php the_field('address_contact', $id) ?></small>
            </div>
        </div>

        <div class="col-md-3 location-information">
            <div class="d-flex phones">
                <div class="phone-icon mr-3"><?php get_template_part('icons/icon', 'phone'); ?></div>
                <div class="phone-numbers">
                   <?php the_field('phone_contact'); ?>
                </div>
            </div>
            <hr class="separator">
            <div class="d-flex mail align-items-start pt-2">
                <div class="mail-icon mr-3"><?php get_template_part('icons/icon', 'mail'); ?></div>
                <div class="mail">
                    <p><span class="bold">Email:</span> <a href="mailto:<?php the_field('email_contact'); ?>"><?php the_field('email_contact'); ?></a></p>
                </div>
            </div>
            <hr class="separator">
            <div class="d-flex time pt-2">
                <div class="time-icon mr-3"><?php get_template_part('icons/icon', 'time'); ?></div>
                <div class="hours">
                    <p><span class="bold">Hours of operation</span></p>
                    <?php the_field('hours_of_operations', $id); ?>
                </div>
            </div>

        </div>

        <div class="col-md-6 location-hero-image">
            <img src="<?php the_field('image_contact_us'); ?>" alt="Contact Us" class="w-100">
        </div>
    </div>
</div>

<section class="container-contact-form py-4 container-fluid px-md-5 px-3">
    <div class="row">
        <div class="col-md-5 m-auto">
            <h2 class="title-contact-form"><?php the_field('title_answer_contact_us'); ?></h2>
            <p><?php the_field('text_area_contact_us'); ?></p>
        </div>
        <div class="col-md-7">
            <div class="form-container">
                <div class="form-header">
                    <p class="bold mb-0">Contact Us</p>
                </div>
                <div class="form-body py-4 px-md-5 px-2">
                <?php echo do_shortcode('[contact-form-7 id="11" title="Contact Us"]');?>
                </div>
            </div>
           
        </div>
    </div>
</section>

<?php get_footer(); ?>