<?php
/**
 * The template for displaying 404 pages (not found)
 */
get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

			<div class="error-404 not-found py-5">
				<div class="container py-5">
                    <div class="d-flex flex-column align-items-center justify-content-center">
                        <header>
                            <h1><span class="big">404</span> <br>
                            PAGE NOT FOUND
                            </h1>
                        </header>
                        <p>The page you were looking for could not be found. <a href="<?php echo get_home_url(); ?>" class="green">Go to Home Page</a></p>
                    </div>
                </div>
			</div><!-- .error-404 -->

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
