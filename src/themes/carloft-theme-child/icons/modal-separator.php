<svg xmlns="http://www.w3.org/2000/svg" width="173" height="644" viewBox="0 0 173 644">
    <g fill="none" fill-rule="evenodd">
        <g fill="#BFD440">
            <g>
                <g>
                    <g>
                        <path d="M99.506 0L0 644 40.586 644 138 0z" opacity=".347" transform="translate(-380 -1088) translate(1) translate(30 1088) translate(349)"/>
                        <path d="M137.718 0L40 644 73.722 644 173 0z" opacity=".895" transform="translate(-380 -1088) translate(1) translate(30 1088) translate(349)"/>
                    </g>
                </g>
            </g>
        </g>
    </g>
</svg>
