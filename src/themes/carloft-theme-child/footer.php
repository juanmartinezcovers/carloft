<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since Twenty Nineteen 1.0
 */

?>

	</div><!-- #content -->
<section class="footer">
	<div class="container-fluid custom-footer-padding">
    <div class="row">


      <div class="col-md-3 col-12 py-md-4 py-0 mb-3 mb-md-0">
        <div class="footer-logo">
          <img src="<?php the_field('logo_footer', 'option'); ?>" alt="carloft" class="logo">
        </div>
        <div class="footer-info">
          <p>
            <?php the_field('content_text_footer' ,'option'); ?>
          </p>
        </div>
        <div class="footer-social mt-3">
          <?php get_template_part('template-parts/header/social', 'header'); ?>
        </div>
      </div>

      <div class="col-md-2 offset-md-1">
        <div class="contact-info">
          <h3>Menu</h3>
          <nav>
          <?php
            wp_nav_menu( array(
              'theme_location' => 'menu-1',
              'menu_id'		 => '',
              'container'		 => false,
              'fallback_cb'	 => false,
              'menu_class'	 => 'carloft-menu-footer', )
            );
			    ?>
          </nav>
        </div>
      </div>

      <div class="col-md-3 col-12 mb-3 mb-md-0">
        <div class="contact-info">
          <h3>Contact</h3>

            <?php    $i = 0; if( have_rows('repeater_contact_footer','option') ):
                while( have_rows('repeater_contact_footer', 'option') ) : the_row();

                 
                    $title_repeater_footer = get_sub_field('title_repeater_contact_footer', 'option'); 
                    $content_repeater_footer = get_sub_field('content_repeater_contact_footer', 'option'); ?>

                <p class="contact-<?php echo $i;  ?>">
                  <span class="contact-title"><?php echo $title_repeater_footer ?> </span> <br>
                    <?php echo $content_repeater_footer ?> 
                </p>

                <?php $i++; endwhile;
            endif; ?>

        </div>
      </div>

      <div class="col-md-3 second-column">
        <div class="contact-info">

        </div>
      </div>
      <div class="d-flex align-items-center justify-content-center w-100 down-links">
  <a href="/terms-conditions/" class="mr-3">TERMS & CONDITIONS</a>
  <div>|</div>
  <a href="/privacy-policy/" class="ml-3">PRIVACY POLICY</a>
</div>

<div class="navegation-footer-mobile d-none">
  <img src="<?php echo get_stylesheet_directory_uri() . '/icons/iconfooternav.png' ?>" alt="">
</div>

</section>

<div class="container-fluid copyright py-3">
    <div class="row">
      <div class="col-6 mx-auto">
        <p><?php the_field('footer_copyright', 'option'); ?> <span><?php echo date('Y'); ?></span></p>
      </div>
    </div>
  </div>
</div><!-- #page -->

<script>
let button = $('.navegation-footer-mobile');
button.on("click", function(){
 topFunction();
});
function topFunction() {
  $("html, body").animate({ scrollTop: 0 }, 500);
}
</script>

<?php wp_footer(); ?>
<script src="https://cdn.gubagoo.io/toolbars/104069/loader_104069_1.js" async></script>
</body>
</html>