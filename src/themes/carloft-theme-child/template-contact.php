<?php  
	 /* Template Name: Contact */
	get_header();
?> 

<div id="contact-map" class="map-container">

    <!-- <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d1409.11633568865!2d-74.770834!3d45.060791!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x3570629535fa88a0!2sCarloft!5e0!3m2!1ses-419!2sus!4v1601605735037!5m2!1ses-419!2sus" width="100%" height="621" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe> -->
    <div id="map"></div>
    <div class="locations">
        <div class="d-flex align-items-center justify-content-center py-2 px-5 logo-container">
            <img src="<?php echo get_stylesheet_directory_uri() . '/dist/images/logo_carloft.png' ?>" alt="carloft" class="w-100">
        </div>
        <div class="body-locations" id="pac-card">
            <div class="input-icon">
            <?php 
            get_template_part( 'icons/icon', 'search' );?>
                <input
                type="text"
                placeholder="City or Postal Code"
                id="pac-input" 
                class="input-field"/>
            </div>
        </div>

        <div class="py-2 body-locations">
        <?php //Locations Array 
        
           $locations = [
               'Cornwall',
               'Kingston',
               'Napanee',
               'Carleton'
           ];
           
           foreach( $locations as $location ):
        ?>
          <div class="content-location d-flex align-items-center">
              <div class="marker-icon mr-3">
                  <?php get_template_part('icons/icon','marker'); ?>
              </div>
              <div class="place">
                  <a href="/contact-form/<?php echo strtolower($location); ?>" class="mb-0"><?php echo $location . ' Location'?></a>
              </div>
          </div>
        <?php endforeach; ?>
        </div>
</div>
<?php get_footer(); ?>