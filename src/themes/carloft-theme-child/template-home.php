<?php  
	 /* Template Name: Home */
	get_header();
?> 


<section id="pre-approved">
	<?php get_template_part( 'template-parts/home/slider', 'cta' ); ?>
</section>

<section class="banner-promotion-desktop">
	<?php get_template_part('template-parts/home/banner', 'promotion'); ?>
</section>	

<section class="we-pay" id="we-pay">
	<?php get_template_part('template-parts/home/we', 'pay'); ?>
</section>

<section class="how-it-works" style="background-color:<?php the_field('color_background_how_it_works'); ?>">
	<?php get_template_part('template-parts/home/how', 'it-works'); ?>
</section>

<section class="carloft-way">	
	<?php get_template_part('template-parts/home/carloft', 'way'); ?>
</section>

<section class="categories">	
	<?php get_template_part('template-parts/home/categories'); ?>
</section>

<section class="w-100 instagram-feed">
	<div class="new-content container"></div>	
	<div class="d-none">
		<?php get_template_part('template-parts/home/instagram'); ?>
	</div>
</section>

<?php get_template_part('template-parts/step-by-step/pop', 'ups'); ?>

<? get_footer(); ?>


