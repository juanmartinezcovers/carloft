<?php 

$brands = get_terms( array(
    'taxonomy' => 'brands',
    'hide_empty' => false,
));

?>

<div class="toggle-container">
    <button class="toggle-collapse" type="button" data-toggle="collapse" data-target="#collapse-filter-2" aria-expanded="true" aria-controls="collapse-filter-1">
        Brand
        <i class="fa" aria-hidden="true"></i>
    </button>
    <hr class="filter-separator">
</div>

<div id="collapse-filter-2" class="collapse scroll show">
    <!-- <label class="category-label small-spacing" for="brand-jeep">
        <img class="brand-img" src="<?php echo get_stylesheet_directory_uri() . '/dist/images/filters/logo-jeep.png'; ?>" alt="Jeep Logo">
        <p><span class="item-name">Jeep</span></p>
    </label>
    <label class="category-label small-spacing" for="brand-audi">
        <img class="brand-img" src="<?php echo get_stylesheet_directory_uri() . '/dist/images/filters/logo-audi.png'; ?>" alt="Audi Logo">
        <p><span class="item-name">Audi</span></p>
    </label>
    <label class="category-label small-spacing" for="brand-ford">
        <img class="brand-img" src="<?php echo get_stylesheet_directory_uri() . '/dist/images/filters/logo-ford.png'; ?>" alt="Ford Logo">
        <p><span class="item-name">Ford</span></p>
    </label>
    <label class="category-label small-spacing" for="brand-mercedes">
        <img class="brand-img" src="<?php echo get_stylesheet_directory_uri() . '/dist/images/filters/logo-mercedes.png'; ?>" alt="Mercedes Logo">
        <p><span class="item-name">Mercedes</span></p>
    </label>
    <label class="category-label small-spacing" for="brand-bmw">
        <img class="brand-img" src="<?php echo get_stylesheet_directory_uri() . '/dist/images/filters/logo-bmw.png'; ?>" alt="BMW Logo">
        <p><span class="item-name">BMW</span></p>
    </label>
    <label class="category-label small-spacing" for="brand-porsche">
        <img class="brand-img" src="<?php echo get_stylesheet_directory_uri() . '/dist/images/filters/logo-porsche.png'; ?>" alt="Porsche Logo">
        <p><span class="item-name">Porsche</span></p>
    </label> -->
    <?php foreach($brands as $term): ?>
    <label class="category-label small-spacing" for="brand-<?php echo $term->slug ?>">
        <p><span class="item-name"><?php echo $term->name  ?></span></p>
    </label>
    <input type="checkbox" id="brand-<?php echo $term->slug ?>" name="make" value="<?php echo $term->name  ?>" 	data-hidden="true">
    <?php endforeach; ?>
</div>
