<?php 

$categories = get_terms( array(
    'taxonomy' => 'category',
    'hide_empty' => false,
));

?>

<div class="toggle-container">
    <button class="toggle-collapse" type="button" data-toggle="collapse" data-target="#collapse-filter-1" aria-expanded="true" aria-controls="collapse-filter-1">
        Categories
        <i class="fa" aria-hidden="true"></i>
    </button>
    <hr class="filter-separator">
</div>

<div id="collapse-filter-1" class="collapse show">

    <?php foreach($categories as $term): ?>
    
        <?php if ($term->slug == 'car'): ?>
            <label class="category-label" for="category-<?php echo $term->slug ?>">
                <img class="category-img" src="<?php echo get_stylesheet_directory_uri() . '/dist/images/filters/icon-cars.png'; ?>" alt="Cars Category">
                <p><span class="item-name">Sedans</span></p>
            </label>
        <?php elseif($term->slug == 'suv'): ?>
            <label class="category-label" for="category-<?php echo $term->slug ?>">
                <img class="category-img" src="<?php echo get_stylesheet_directory_uri() . '/dist/images/filters/icon-suv.png'; ?>" alt="Suv Category">
                <p><span class="item-name">SUVs</span></p>
            </label>
        <?php elseif($term->slug == 'truck'): ?>
            <label class="category-label" for="category-<?php echo $term->slug ?>">
                <img class="category-img" src="<?php echo get_stylesheet_directory_uri() . '/dist/images/filters/icon-trucks.png'; ?>" alt="Trucks Category">
                <p><span class="item-name">Trucks</span></p>
            </label>
        <?php elseif($term->slug == 'van'): ?>
            <label class="category-label" for="category-<?php echo $term->slug ?>">
                <img class="category-img" src="<?php echo get_stylesheet_directory_uri() . '/dist/images/filters/icon-vans.png'; ?>" alt="Vans Category">
                <p><span class="item-name">Vans</span></p>
            </label>
        <?php elseif($term->slug == 'coupe'): ?>
            <label class="category-label" for="category-<?php echo $term->slug ?>">
                <img class="category-img" src="<?php echo get_stylesheet_directory_uri() . '/dist/images/filters/coupe.png'; ?>" alt="Coupe Category">
                <p><span class="item-name">Coupes</span></p>
            </label>
        <?php elseif($term->slug == 'hatchback'): ?>
            <label class="category-label" for="category-<?php echo $term->slug ?>">
                <img class="category-img" src="<?php echo get_stylesheet_directory_uri() . '/dist/images/filters/hatchback2020.png'; ?>" alt="Hatchback Category">
                <p><span class="item-name">Hatchbacks</span></p>
            </label>
        <?php endif; ?>

    <?php endforeach; ?>

    <?php foreach($categories as $term): ?>
        <input type="checkbox" id="category-<?php echo $term->slug ?>" 	name="type" value=<?php echo $term->name ?>  data-hidden="true">
    <?php endforeach; ?>
</div>