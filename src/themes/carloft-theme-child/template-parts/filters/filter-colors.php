<?php 

$colors = get_terms( array(
    'taxonomy' => 'colors',
    'hide_empty' => false,
));

?>

<div class="toggle-container">
    <button class="toggle-collapse" type="button" data-toggle="collapse" data-target="#collapse-filter-3" aria-expanded="true" aria-controls="collapse-filter-1">
        Colour
        <i class="fa" aria-hidden="true"></i>
    </button>
    <hr class="filter-separator">
</div>

<div id="collapse-filter-3" class="collapse show scroll">
    <!-- <label class="category-label small-spacing" for="color-black">
        <span class="circle black"></span>
        <p><span class="item-name">Black</span></p>
    </label>
    <label class="category-label small-spacing" for="color-white">
        <span class="circle white"></span>
        <p><span class="item-name">White</span></p>
    </label>
    <label class="category-label small-spacing" for="color-grey">
        <span class="circle grey"></span>
        <p><span class="item-name">Grey</span></p>
    </label>
    <label class="category-label small-spacing" for="color-red">
        <span class="circle red"></span>
        <p><span class="item-name">Red</span></p>
    </label> -->
    <?php foreach($colors as $term): ?>
    <label class="category-label small-spacing" for="color-<?php echo $term->slug ?>">
        <p><span class="item-name"><?php echo $term->name  ?></span></p>
    </label>
    <input type="checkbox" id="color-<?php echo $term->slug ?>" name="colorexterior" value="<?php echo $term->name ?>" data-hidden="true">
    <?php endforeach; ?>
</div>