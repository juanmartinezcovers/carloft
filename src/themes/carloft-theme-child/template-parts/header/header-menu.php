<div class="container-fluid header-nav">
  <div class="row align-items-center d-none d-md-flex">
    <div class="col-3 logo-container">
      <div class="inner-logo-container">
        <a href="<?php echo home_url(); ?>">
          <img src="<?php the_field('logo_header_', 'option'); ?> ?>" alt="carloft">
        </a>
      </div>
    </div>

    <div class="col-8 menu-container">
      <nav>
      <?php
      wp_nav_menu( array(
				'theme_location' => 'menu-1',
				'menu_id'		 => '',
				'container'		 => false,
				'fallback_cb'	 => false,
				'menu_class'	 => 'carloft-menu', )
			);
			?>
      </nav>
    </div>

  </div>

  <div class="row align-items-center d-flex d-md-none no-gutters">
      <div class="col-3">
       <!-- Top Navigation Menu -->
        <div class="topnav">
          <a href="javascript:void(0);" data-toggle="mobile-menu" class="icon">
            <i class="fa fa-bars"></i>
          </a>

          <nav>
          <?php
            wp_nav_menu( array(
              'theme_location' => 'menu-1',
              'menu_id'		 => '',
              'container'		 => false,
              'fallback_cb'	 => false,
              'menu_class'	 => 'carloft-menu-mobile', )
            );
			    ?>
          </nav>
        </div>
      </div>
      <div class="col-6">
        <div class="inner-logo-container">
          <a href="<?php echo home_url(); ?>">
            <img src="<?php echo get_stylesheet_directory_uri() . '/dist/images/logo_carloft.png' ?>" alt="carloft">
          </a>
        </div>
      </div>
      <div class="col-3">
        -
      </div>
  </div>
</div>

<div class="fixed-mobile d-block d-md-none">
  <div class="wrapper-close">
    <button data-toggle="mobile-menu">+</button>
  </div>
  <nav>
      <?php
        wp_nav_menu( array(
          'theme_location' => 'menu-1',
          'menu_id'		 => '',
          'container'		 => false,
          'fallback_cb'	 => false,
          'menu_class'	 => 'carloft-menu-mobile', )
        );
      ?>
  </nav>
</div>