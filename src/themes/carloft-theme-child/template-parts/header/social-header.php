<?php 

$instagram = get_field('instagram_header_link','option') ;
$facebook = get_field('facebook_header_link','option');
$twitter = get_field('twitter_header_link','option');
$youtube = get_field('youtube_header_link','option');
?>
<ul class="social-top d-md-flex align-items-center justify-content-end d-none mb-0">
  <li class="d-flex align-items-center">
      <label class="dropdown">

        <div class="dd-button">
          <?php get_template_part( 'icons/icon', 'phone' );?>
        </div>

        <input type="checkbox" class="dd-input" id="test">

        <ul class="dd-menu">

          <?php if( have_rows('repeater_number_phone','option') ):
              while( have_rows('repeater_number_phone', 'option') ) : the_row(); ?>

                <li><a href="tel:<?php the_sub_field('repeater_link_phone' , 'option'); ?>"><?php the_sub_field('repeater_text_phone' , 'option'); ?></a></li>

              <?php endwhile;
          endif; ?>

        </ul>
        
      </label>
  </li>
  <?php if( $instagram ):?>
  <li>
    <a href="<?php echo $instagram; ?>" target="_blank"><?php get_template_part('icons/icon', 'instagram');?></a>
  </li>
  <?php endif; ?> 
  <?php if( $facebook ): ?>
  <li>
    <a href="<?php echo $facebook; ?>" target="_blank"><?php get_template_part('icons/icon', 'facebook');?></a>
  </li>
  <?php endif; ?>
  <?php if ( $twitter ): ?>
  <li>
    <a href="<?php echo $twitter; ?>" target="_blank"><?php get_template_part('icons/icon', 'twitter');?></a>
  </li>
  <?php endif; ?>
  <?php if( $youtube ): ?>
  <li>
    <a href="<?php echo $youtube; ?>" target="_blank"><?php get_template_part('icons/icon', 'youtube');?></a>
  </li>
  <?php endif; ?>
</ul>




