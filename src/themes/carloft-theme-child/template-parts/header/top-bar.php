<div class="top-bar py-3">
  <div class="container-fluid container-padding">
    <div class="row justify-content-between align-items-center d-none d-md-flex">
      
      <div class="col-md-5">
        <div class="top-search-bar">
          <?php get_template_part( 'template-parts/header/search', 'drawer' ); ?>
        </div>
      </div>

      <div class="col-md-5 right-top-element">
         <?php get_template_part('template-parts/header/social', 'header'); ?>
      </div>

    </div>

    <div class="row d-md-none d-flex">
      <div class="col-6 mx-auto">
        <div class="d-flex align-items-center mobile-phone-number social-top justify-content-center">
          <label class="dropdown-mobile">

            <div class="dd-button">
              <?php get_template_part( 'icons/icon', 'phone' );?>
            </div>

            <input type="checkbox" class="dd-input" id="test">

            <ul class="dd-menu">

              <?php if( have_rows('repeater_number_phone','option') ):
                  while( have_rows('repeater_number_phone', 'option') ) : the_row(); ?>

                    <li><a href="<?php the_sub_field('repeater_link_phone' , 'option'); ?>"><?php the_sub_field('repeater_text_phone' , 'option'); ?></a></li>

                  <?php endwhile;
              endif; ?>

            </ul>

          </label>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="search-mobile d-none">
  <div class="container">
    <div class="row mx-auto">
      <div class="col-12">
        <?php get_template_part( 'template-parts/header/search', 'drawer' ); ?>
      </div>
    </div>
  </div>
</div>

