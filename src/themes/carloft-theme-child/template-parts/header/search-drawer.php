<form class="top-search" action='<?php bloginfo('url'); ?>/filters-inventory' method="post" role="search">
 <div class="d-flex">
  <div class="input-icon">
    <?php 
    get_template_part( 'icons/icon', 'search' );?>
    <input
    type="text"
    placeholder="Search"
    name="inventory_search"
    value=""
    class="input-field"
  />
  </div>
  <input type="submit" value="Search" class="btn-search submit ml-3 d-none" />
 </div>
</form>