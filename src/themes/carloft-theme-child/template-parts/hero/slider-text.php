<div class="slider-text-container d-none d-md-block">
  <div class="car-big">
    <span>car</span>
  </div>
  <div class="big-text-slider">
    <div class="slide-text">
      <span class="services">services</span>
    </div>
    <div class="slide-text">
      <span class="shop">shop</span>
    </div>
    <div class="slide-text">
      <span class="trade-in">trade in</span>
    </div>
    <div class="slide-text">
      <span class="loft">loft</span>
    </div>
  </div>
</div>