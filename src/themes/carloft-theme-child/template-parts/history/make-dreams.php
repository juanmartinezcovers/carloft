<div class="container-fluid make-dreams" style="background-image:url('<?php the_field('background_midle_banner_history'); ?>');">
    <div class="row">
        <div class="content" style="padding:100px;">
            <p class="font-weight-bold mb-0"><?php the_field('content_midle_banner_history'); ?></p>
            <a href="<?php the_field('button_link_midle_banner_history'); ?>"><button><?php the_field('button_text_midle_banner_history'); ?></button></a>
        </div>
    </div>

</div>
