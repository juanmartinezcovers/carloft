<div class="container">
    <div class="row">
        <div class="col-lg-4 col-sm-12 title-our-clients">
            <p><?php the_field('title_our_clients');?></p>
        </div>
        <div class="col-lg-8 col-sm-12 logos-our-clients">
            <img src="<?php the_field('image_our_clients');?>" alt="">
        </div>
    </div>
</div>