<div class="container py-5">
    <div class="row">
        <div class="col-md-4">
            <div class="title-our-donations"><p><?php the_field('title_our_donations'); ?></p></div>
            <p class="content-our-donations"><?php the_field('content_our_donations'); ?></p>
            <p class="percent-our-donations"><?php the_field('title_bottom_our_donations'); ?></p>
        </div>
        <div class="col-md-8">

           <div class="our-donation-slider px-2">


            <?php $i=1; if( have_rows('repeater_slider_our_donations') ):
                  while( have_rows('repeater_slider_our_donations') ) : the_row();

                    $color_donations = get_sub_field('color_repeater_our_donations');
                    $title_donations = get_sub_field('title_repeater_our_donations');
                    $subtitle_donations = get_sub_field('subtitle_repeater_our_donations');
                    $number_donations = get_sub_field('number_repeater_our_donations');
                    $image_donations = get_sub_field('image_repeater_our_donations'); ?>

                    <div class="donation-slide">
                      <div class="header-slider d-flex" style="background:<?php echo $color_donations ?>">
                        <div class="dash m-auto"></div>
                        <div class="content-header-slider m-auto">
                          <p class="header-title mb-0"><?php echo  $title_donations ?></p>
                          <small class="d-block"><?php echo $subtitle_donations ?></small>
                        </div>
                        <p class="number">0<?php echo $i;?></p>
                      </div>
                      <img src="<?php echo $image_donations ?> " alt="">
                    </div>                   

                  <?php $i++; endwhile;
              endif; ?>

           </div>

           <div class="controls-slick-customs d-none d-md-flex">
             <div class="prev-custom arrow mr-4"><?php get_template_part('icons/icon', 'chevron-left'); ?></div>
             <div class="next-custom arrow mr-4"><?php get_template_part('icons/icon', 'right-arrow'); ?></div>
           </div>
        </div>
    </div>
</div>