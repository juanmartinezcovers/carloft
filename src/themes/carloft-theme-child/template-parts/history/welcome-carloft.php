<div class="container">
    <div class="row main-welcome-carloft">
        <div class="col-lg-1 col-md-2 d-none d-md-block d-lg-block about-story m-auto">
            <div class="vertical-content">
                <hr>
                <div class="vertical">
                    <?php the_field('gray_title_welcome'); ?>
                </div>             
            </div>
            <!-- <img src="php the_field('image_title_welcome'); ?>" alt=""> -->
        </div>
        <div class="col-lg-4 col-md-10 col-sm-12 image-welcome-carloft m-auto" style="background-image:url('<?php the_field('image_welcome'); ?>');">
            <img src="<?php echo get_stylesheet_directory_uri() . '/icons/lines.png' ?>" alt="">
        </div>
        <div class="col-lg-7 col-md-12 col-sm-12 content-welcome-carloft m-auto">
            <h2><?php the_field('title_welcome');?></h2>
            <p><?php the_field('content_welcome'); ?></p>
        </div>
    </div>
</div>
