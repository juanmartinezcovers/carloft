<div class="step-page">
    <div class="d-flex flex-column align-items-center justify-content-center">
        <h3>WHERE DO YOU <span class="green oswald">LIVE?</span></h3>
        <p>We find you the best options closest to your home</p>

        <fieldset class="location-input w-100 my-3 my-md-5">
          <div class="d-flex justify-content-center income-container">

            <div class="input-container">
                    <!-- <input type="text" id="locationStep" name="locationStep" value="" placeholder="ENTER HOME ADDRESS">
                     -->
                    <input type="text" name="stepLocation" id="stepLocation" placeholder="ENTER HOME ADDRESS">
                    <label for="locationStep"><?php get_template_part('icons/icon', 'world'); ?></label>
            </div>

          </div>
      </fieldset>     
    </div>
</div>