<div class="step-page">
    <div class="d-flex flex-column align-items-center justify-content-center">
        <h3>DO YOU HAVE A VEHICLE TO <span class="green oswald">TRADE IN?</span></h3>
        <p>We pay top dollar for your current vehicle</p>

        <fieldset class="boxed w-100 my-3 my-md-5">
          <div class="d-flex justify-content-center">

            <div class="input-container mr-md-4 mr-0">
                    <input type="radio" id="trade" name="trade" value="Yes">
                    <label for="trade">YES</label> 
            </div>

            <div class="input-container">
                <input type="radio" id="no-trade" name="trade" value="No">
                <label for="no-trade">NO</label>
            </div>

          </div>
      </fieldset>     
    </div>
</div>