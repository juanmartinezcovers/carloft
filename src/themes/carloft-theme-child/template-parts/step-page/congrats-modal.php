<!-- Modal Container -->
<div class="modal fade" id="congrats" tabindex="-1" role="dialog" aria-labelledby="congrats" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      <div class="modal-body">

        <div class="container-fluid p-0" data-modal="step-form">
            <div class="congrats-modal py-4 px-3 px-md-5 d-flex flex-column align-items-center justify-content-center">
              
              <img src="<?php echo get_stylesheet_directory_uri() . '/icons/circle.svg' ?>" alt="">

              <h3>CONGRATULATIONS</h3>

              <div class="input-container mb-3 d-none">
                <input type="text" id="location" name="location" value="" placeholder="ENTER HOME ADDRESS">
                <label for="location"><?php get_template_part('icons/icon', 'world'); ?></label>
              </div>

              <p><span class="bold">We will </span> email you your confirmation</span></p>
              <form id="congrats-form" class="w-100">
                <div class="row">
                  <div class="col-md-6 mb-3">
                    <div class="input-container">
                      <input type="text" name="firstName" id="firstName-Congrats" placeholder="First Name*" required>
                    </div>
                  </div>
                  <div class="col-md-6 mb-3">
                    <div class="input-container">
                      <input type="text" name="lastName" id="lastName-Congrats" placeholder="Last Name*" required>
                    </div>
                  </div>
                  <div class="col-12 mb-3">
                    <div class="input-container">
                      <input type="email" name="email" id="email-Congrats" placeholder="Email Address*" required>
                    </div>
                  </div>
                  <div class="col-12 mb-3">
                    <div class="input-container">
                      <input type="text" name="phone" id="phone-Congrats" placeholder="Phone Number" required>
                    </div>
                  </div>
                  <small class="d-block disclaimer px-3">
                  We do not share any of your information with anyone else. We use this information only for the purpose of finding you a vehicle.
                  </small>
                </div>
                <button type="submit" class="d-block mx-auto px-5 mt-3">Submit</button>
              </form>
            </div>
        </div>
        
      </div>
    </div>
  </div>
</div>