<div class="step-page">
    <div class="d-flex flex-column align-items-center justify-content-center">
        <h3>HOW LONG HAVE YOU BEEN RECEIVING<span class="green oswald">YOUR INCOME</span></h3>
        <p>This helps us with your approval</p>
        <fieldset class="numbers my-3 my-md-5">
          <div class="d-flex justify-content-center">

            <div class="input-container mr-md-4 mr-0">
                    <input type="number" id="years" name="income-duration" value="">
                    <label for="years">YEARS</label> 
            </div>

            <div class="input-container">
                <input type="number" id="months" name="income-duration" value="">
                <label for="months">MONTHS</label>
            </div>

          </div>
      </fieldset>     
    </div>
</div>