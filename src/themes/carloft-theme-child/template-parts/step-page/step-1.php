<div class="step-page">
    <div class="d-flex flex-column align-items-center justify-content-center">
        <h3>TELL US YOUR <span class="green oswald">BUDGET</span></h3>
        <p>Pick your monthly payment</p>

        <fieldset class="boxed w-100 my-3 my-md-5">
          <div class="d-flex justify-content-between">
          <div class="input-container">
                <input type="radio" id="low" name="budget" value="$50 - $250">
                <label for="low">$50 - $250</label>
            </div>

           <div class="input-container">
                <input type="radio" id="medium" name="budget" value="$250 - $375">
                <label for="medium">$250 - $375</label> 
           </div>

            <div class="input-container">
                <input type="radio" id="high" name="budget" value="$375 - $500">
                <label for="high">$375 - $500</label> 
            </div>

            <div class="input-container">
                <input type="radio" id="over500" name="budget" value="Over $500">
                <label for="over500">over $500</label> 
            </div>
          </div>
      </fieldset>     
    </div>
</div>