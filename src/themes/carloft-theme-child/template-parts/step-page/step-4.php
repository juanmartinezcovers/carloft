<div class="step-page">
    <div class="d-flex flex-column align-items-center justify-content-center">
        <h3>TELL US ABOUT YOUR <span class="green oswald">INCOME</span></h3>
        <p>All income sources are welcome</p>

        <fieldset class="boxed w-100 my-3 my-md-5">
          <div class="d-flex justify-content-center income-container">

            <div class="input-container mr-md-4 mr-0">
                    <input type="radio" id="hourly" name="income" value="Hourly">
                    <label for="hourly">I GET PAID HOURLY</label> 
            </div>

            <div class="input-container mr-md-4 mr-0">
                <input type="radio" id="salary" name="income" value="Salary">
                <label for="salary">I HAVE A SALARY</label>
            </div>

            <div class="input-container mr-md-4 mr-0">
                <input type="radio" id="retired" name="income" value="Retired">
                <label for="retired">I AM RETIRED</label>
            </div>

            <div class="input-container mr-md-4 mr-0">
                <input type="radio" id="unsure" name="income" value="Unsure">
                <label for="unsure">I AM UNSURE</label>
            </div>

          </div>
      </fieldset>     
    </div>
</div>