<div class="step-page">
    <div class="d-flex flex-column align-items-center justify-content-center">
        <h3>ARE YOU CURRENTLY <span class="green oswald">EMPLOYED?</span></h3>
        <p>This helps us determine the best financing options for you</p>

        <fieldset class="boxed w-100 my-3 my-md-5">
          <div class="d-flex justify-content-center">

            <div class="input-container mr-md-4 mr-0">
                    <input type="radio" id="full-time" name="job" value="Full Time">
                    <label for="full-time">FULL TIME</label> 
            </div>

            <div class="input-container mr-md-4 mr-0 ">
                <input type="radio" id="part-time" name="job" value="Part time">
                <label for="part-time">PART TIME</label>
            </div>

            <div class="input-container">
                <input type="radio" id="not-working" name="job" value="Not Working">
                <label for="not-working">NOT WORKING</label>
            </div>

          </div>
      </fieldset>     
    </div>
</div>