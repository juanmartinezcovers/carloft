<div class="form-container d-none" data-form="tab">
    <div class="form-steps py-4 px-5">
        <p class="bold mb-0">Step 2</p>
        <small class="d-block mb-3">Any other information for us? The more information we have the more accurate the quote can be.</small>
        <div class="w-100 input-container d-flex mb-3 align-items-start">
            <img src="<?php echo get_stylesheet_directory_uri() . '/dist/images/comment.png' ?>" alt="user" class="icon-input">
            <textarea name="message" id="message-step" cols="30" rows="5" placeholder="Message"></textarea>
        </div>
    </div>

    <div class="submit-form-steps px-5 mt-n3 mb-5">
        <input type="button" value="Back" data-form="back" class="mr-2">
        <input type="submit" value="Continue" data-form="submit">
    </div>    
</div>