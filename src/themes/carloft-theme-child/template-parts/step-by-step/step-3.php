<div class="form-container d-none" data-form="tab">
    <div class="form-steps py-4 px-5">
        <p class="bold mb-0">Step 3 <br>
        All done!
        </p>
        <small class="d-block mb-3">Where we can send the quote</small>
        <div class="w-100 input-container d-flex mb-3 align-items-center">
            <img src="<?php echo get_stylesheet_directory_uri() . '/dist/images/keys.png' ?>" alt="user" class="icon-input">
            <input type="text" class="text w-100" placeholder="Seller's name" id="seller">
        </div>
        <div class="w-100 input-container d-flex mb-3 align-items-center">
            <img src="<?php echo get_stylesheet_directory_uri() . '/dist/images/phone.png' ?>" alt="model" class="icon-input">
            <input type="text" class="text w-100" placeholder="Phone number" id="phone-step">
        </div>
        <div class="w-100 input-container d-flex mb-3 align-items-center">
            <img src="<?php echo get_stylesheet_directory_uri() . '/dist/images/mail.png' ?>" alt="year" class="icon-input icon-fill">
            <input type="email" class="text w-100" placeholder="Email" id="email-step" required>
        </div>
    </div>

    <div class="submit-form-steps px-5 mt-n3 mb-5">
        <input type="button" value="Back" data-form="back" class="mr-2">
        <input type="submit" value="Continue" data-form="submit">
    </div>
</div>