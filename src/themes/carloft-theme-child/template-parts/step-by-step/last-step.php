<div class="d-flex flex-column align-items-center justify-content-center py-5">
    <img src="<?php echo get_stylesheet_directory_uri() . '/dist/images/icon-last-step.png'?>" alt="" class="mb-3">
    <h3 class="oswald">Thank you! We will be in touch right away with <br>
    <span class="green">your quote! </span></h3>
</div>