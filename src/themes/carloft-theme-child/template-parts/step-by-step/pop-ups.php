<!-- Modal Container -->
<div class="modal fade" id="step-by-step" tabindex="-1" role="dialog" aria-labelledby="step-by-step" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      <div class="modal-body">

        <div class="container-fluid p-0" data-modal="step-form">
            <div class="row no-gutters">
                <div class="col-md-5 image-modal" style="background: url('<?php echo get_stylesheet_directory_uri() . '/dist/images/modal-image.png' ?>');">
                    <div class="separator-modal">
                      <?php get_template_part('icons/modal', 'separator'); ?>
                    </div>
                </div>

                <div class="col-md-7 py-5 form-wrapper">
                <?php get_template_part( 'template-parts/step-by-step/step', '1' );  
                    get_template_part( 'template-parts/step-by-step/step', '2' ); 
                    get_template_part( 'template-parts/step-by-step/step', '3' );
                    ?>
                  </div>
                </div>
            </div>
        </div>

        <div class="last-step d-none" data-form="last-step">
          <?php get_template_part('template-parts/step-by-step/last', 'step'); ?>
        </div>
      </div>
    </div>
  </div>
</div>
