<div class="form-container"  data-form="tab">
    <div class="form-steps py-4 px-5">
        <p class="bold mb-0">Step 1</p>
        <small class="d-block mb-3">In order to offer you top dollar for your vehicle, we need a few pieces of information</small>
        <div class="w-100 input-container d-flex mb-3 align-items-center">
            <img src="<?php echo get_stylesheet_directory_uri() . '/dist/images/user.png' ?>" alt="user" class="icon-input">
            <input type="text" class="text w-100" placeholder="Make" id="make">
        </div>
        <div class="w-100 input-container d-flex mb-3 align-items-center">
            <img src="<?php echo get_stylesheet_directory_uri() . '/dist/images/transports.png' ?>" alt="model" class="icon-input">
            <input type="text" class="text w-100" placeholder="Model" id="model">
        </div>
        <div class="w-100 input-container d-flex mb-3 align-items-center">
            <img src="<?php echo get_stylesheet_directory_uri() . '/dist/images/fill.png' ?>" alt="year" class="icon-input icon-fill">
            <input id="year" type="number" max="<?php echo date("Y"); ?>" min="2000" class="text w-100" placeholder="Year">
        </div>
        <div class="w-100 input-container d-flex mb-3 align-items-center">
            <img src="<?php echo get_stylesheet_directory_uri() . '/dist/images/fill.png' ?>" alt="year" class="icon-input icon-fill">
            <input id="km" type="number" min="0" class="text w-100" placeholder="KM">
        </div>
    </div>

    <div class="submit-form-steps px-5 mt-n3 mb-5">
        <button type="submit" value="Continue" data-form="submit">Continue</button>
    </div>
</div>