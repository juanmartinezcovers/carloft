<?php $link = get_field('subtitle_carloft_way_link'); ?>	
	<div class="container">
		<div class="row shadow-carloft-way">
			<div class="carloft-way-title col-lg-4 col-sm-12 col-md-12">
				<h2><?php the_field('title_carloft_way'); ?></h2>
			</div>
			<div class="carloft-way-content col-lg-4 col-sm-12 col-md-12">
				<img class="number" src="<?php echo get_stylesheet_directory_uri() . '/icons/01.png' ?>" alt="carloft">
				<p class="text"><?php the_field('content_carloft_way'); ?></p>
				<div class="bottom d-flex">
				<a href="<?php echo $link['url']; ?>"><p><?php the_field('subtitle_carloft_way');?></p></a>
					<div class="dash"></div>
				</div>
			</div>
			<div class="carloft-way-image m-auto">
				<img src="<?php the_field('image_carloft_way');?>" alt="">
			</div>
		</div>
	</div>

