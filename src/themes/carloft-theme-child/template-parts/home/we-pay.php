<div class="we-pay-title container">
	<h2><?php the_field('title_we_pay'); ?></h2>
</div>
<div class="container">
	<div class="we-pay-subtitle">
		<div class="row content-subtitle">
			<div class="we-pay-line col-1 m-auto">
				<div class="dash"></div>
			</div>
			<div class="we-pay-text col-8 m-auto">
				<h2 class="mb-0"><?php the_field('subtitle_we_pay'); ?></h2>
			</div>
			<div class="we-pay-number col-3 m-auto">
				<img src="<?php echo get_stylesheet_directory_uri() . '/icons/01.png' ?>" alt="carloft">
			</div>
		</div>		
	</div>
	<div class="we-pay-content">
		<img src="<?php the_field('image_we_pay'); ?>" alt="">
		<button type="button" data-toggle="modal" data-target="#step-by-step"><?php the_field('text_button_we_pay'); ?></button>
	</div>
</div>
