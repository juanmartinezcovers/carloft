<div class="container">
    <h3 class="pc"><?php the_field('title_categories_section'); ?></h3>
    <h3 class="mobile"><?php the_field('title_categories_section'); ?></h3>    
    <div class="row d-block d-md-flex">
        <?php
            $truck_title = get_field('trucks_title_category');
            $sedan_title = get_field('sedan_title_category');
            $suvs_title = get_field('suvs_title_category');
            $vans_title = get_field('vans_title_category');
            $truck_back = get_field('trucks_image_category');
            $sedan_back = get_field('sedan_image_category');
            $suvs_back = get_field('suvs_image_category');
            $vans_back = get_field('vans_image_category');
            $truck_icon = get_field('trucks_icon_category');
            $vans_icon = get_field('vans_icon_category');
            $sedan_icon = get_field('sedan_icon_category');
            $suvs_icon = get_field('suvs_icon_category');
            $data=[];
            $change_side=true;
            $data[]=(object)
            [
                "img"=>$truck_back,
                "title"=>$truck_title,
                "svg"=> $truck_icon,
                "description"=>"",
                "url"=>"#truck",
            ];
            $data[]=(object)
            [
                "img"=>$sedan_back,
                "title"=>$sedan_title,
                "svg"=> $sedan_icon,
                "description"=>"",
                "url"=>"#sedan"
            ];   
            $data[]=(object)
            [
                "img"=>$suvs_back,
                "title"=>$suvs_title,
                "svg"=> $suvs_icon,
                "description"=>"",
                "url"=>"#suv",
            ];   
            $data[]=(object)
            [
                "img"=>$vans_back,
                "title"=>$vans_title,
                "svg"=>$vans_icon,
                "description"=>"",
                "url"=>"#van",
            ];                  
            
            $html="";            
            $dots_category_mobile='';
            $card_category_mobile='';
            foreach($data as $count=>$card)
            {        
                $order_img='order-2';
                $order_title='order-1';
                switch($count)
                {
                    case 1: case 2:
                        $order_img='order-1';
                        $order_title='order-2';                        
                    break;
                }
                $category="";
                $html.='<div class="col-3 p-0">';
                    $category.='<div class="categories">';
                        $category.='<div class="content d-flex '.$order_title.'">';
                            $category.='<div class="line-categories col-2 m-auto">';
                                $category.='<div class="dash"></div>';
                            $category.='</div>';
                            $category.='<div class="title-categories col-6 m-auto">';
                                $category.='<h4 class="mb-0">'.$card->title.'</h4>';
                                $category.='<a href="/filters-inventory/'.$card->url.'">See More</a>';
                            $category.='</div>';
                            $category.='<div class="number-categories col-4 m-auto text-right">';
                                $category.='<p class="mb-0">0'.($count+1).'</p>	';
                            $category.='</div>';
                        $category.='</div>';
                        $category.='<div class="image '.$order_img.'" style="background-image: url('.$card->img.');">';
                            $category.='<div class="hover-contain">';
                                $category.='<img src="'.$card->svg.'">';
                                $category.='<h5 class="mb-0 sub-title">'.$card->title.'</h5>';
                                $category.='<p>'.$card->description.'</p>';
                            $category.='</div>';
                        $category.='</div>';
                    $category.='</div>';  
                $html.=$category.'</div>';   

                /* MOBILE  */                
                $card_category_mobile.='<div class="carloft-slice-mobile">';
                $card_category_mobile.=$category;
                $card_category_mobile.='</div>';
                $dots_category_mobile.='<li class="carloft-dot" onclick="carloft_animate_scroll('.$count.')">&nbsp;</li>';
            }
        ?>        
        <div class="category-pc w-100"><?php echo $html ?></div>

        <div class="swipe-by-category d-block d-md-none">
            <?php echo $card_category_mobile; ?>
        </div>
    </div>
</div>
<script>
// function carloft_animate_scroll(position)
// {
//   var size=document.querySelector(".carloft-slice").offsetWidth;
//   document.querySelector(".custom-carloft-slider").scrollLeft=size*position;
//   carloft_start_slider(position);
// }
// function carloft_start_slider(position)
// {
//   var total=document.querySelectorAll(".carloft-dot").length;
//   document.querySelectorAll(".carloft-dot").forEach(function(Element,index)
//   { 
//     if(index==position)
//     {
//       Element.classList.add("actived");
//     }
//     else
//     {
//       Element.classList.remove("actived");
//     }
//   });   
// }
// carloft_start_slider(0);
</script>
