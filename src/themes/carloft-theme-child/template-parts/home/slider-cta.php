<section class="container-slider-cta container py-5">
    <div class="d-flex flex-column align-items-center justify-content-center">
        <h3 class="slider-cta-title">
            <?php echo get_field('title_step_by_step'); ?>
        </h3>
        <h6 class="slider-cta-subheading"><?php the_field('subtitle_step_by_step'); ?></h6>
    </div>

   <div class="slider-cta-wrapper d-none d-md-block">
        <div id="carousel">
            <img src="<?php the_field('trucks_image');?>" alt="" class="cloud9-item" data-car_type="truck">
            <img src="<?php the_field('cars_image');?>" alt="" class="cloud9-item" data-car_type="sedan">
            <img src="<?php the_field('vans_image');?>" alt="" class="cloud9-item" data-car_type="van">
            <img src="<?php the_field('suvs_image');?>" alt="" class="cloud9-item" data-car_type="suv">
        </div>

        <div id="slider-buttons" class="d-none d-md-flex">
            <button class="left"><?php get_template_part('icons/icon', 'chevron-left'); ?></button>
            <button class="right"><?php get_template_part('icons/icon', 'chevron-right'); ?></button>
        </div>
   </div>

   <div class="slider-cta-button align-items-center justify-content-center mt-n5 d-none d-md-flex">
       <!-- <button type="button" data-toggle="modal" data-target="#step-by-step">I WANT A TRUCK</button> -->
       <a href="/how-it-works/#main-steps-aprobation" ><button>I WANT A <span data-car-type=""></span></button></a>
   </div>



   <div class="slider mt-5 d-block d-md-none ">
        <a href="/how-it-works/#main-steps-aprobation" data-car_type="truck"><img src="<?php the_field('trucks_image');?>" alt=""></a>
        <a href="/how-it-works/#main-steps-aprobation" data-car_type="sedan"><img src="<?php the_field('cars_image');?>" alt=""></a>
        <a href="/how-it-works/#main-steps-aprobation" data-car_type="van"><img src="<?php the_field('vans_image');?>" alt=""></a>
        <a href="/how-it-works/#main-steps-aprobation" data-car_type="suv"><img src="<?php the_field('suvs_image');?>" alt=""></a>
   </div>
   <div class="slider-cta-button-mobile align-items-center justify-content-center d-flex d-md-none mt-5">
   <a href="/how-it-works/#main-steps-aprobation" ><button>I WANT A <span data-car-type-mobile="">TRUCK</span></button></a>
   </div>
  
</section>