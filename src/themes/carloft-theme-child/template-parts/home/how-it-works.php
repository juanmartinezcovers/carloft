<div class="container">
	<div class="how-it-works-title">
		<h2><?php the_field('title_how_it_works'); ?></h2>
		<p><?php the_field('subtitle__how_it_works'); ?></p>
	</div>
	<div class="row how-it-works-content d-none d-md-flex">

		<?php if( have_rows('repeater_how_it_works') ):
			while( have_rows('repeater_how_it_works') ) : the_row();
				$icon_works= get_sub_field('icon_repeater_works'); 
				$title_works= get_sub_field('title_repeater_works'); 
				$content_works= get_sub_field('content_repeater_works'); 
				$text_button_works= get_sub_field('text_button_repeater_works'); 
				$link_button_works= get_sub_field('link_button_repeater_works'); ?>

				<div class="how-it-works-mobile col-lg-4 col-sm-12">
					<img src="<?php echo $icon_works ?>" alt="">
					<h3><?php echo $title_works ?></h3>
					<p><?php echo $content_works ?></p>
					<?php if( $text_button_works === 'SELL MY CAR' || $text_button_works === 'VALUE MY TRADE'): ?>
						<button data-toggle="modal" data-target="#step-by-step"><?php echo $text_button_works ?></button>
					<?php else: ?>
						<a href="<?php echo $link_button_works ?>"><button><?php echo $text_button_works ?></button></a>
					<?php endif;?>
				</div>

			<?php endwhile;
		endif; ?>

	</div>

	<div class="row how-it-works-content how-it-works-slider d-flex d-md-none">

		<?php if( have_rows('repeater_how_it_works') ):
			while( have_rows('repeater_how_it_works') ) : the_row();
				$icon_works= get_sub_field('icon_repeater_works'); 
				$title_works= get_sub_field('title_repeater_works'); 
				$content_works= get_sub_field('content_repeater_works'); 
				$text_button_works= get_sub_field('text_button_repeater_works'); 
				$link_button_works= get_sub_field('link_button_repeater_works'); ?>

				<div class="col-lg-4 col-sm-12">
					<img class="mx-auto" src="<?php echo $icon_works ?>" alt="">
					<h3><?php echo $title_works ?></h3>
					<p><?php echo $content_works ?></p>
					<?php if( $text_button_works === 'SELL MY CAR' ): ?>
						<button class="mb-4" data-toggle="modal" data-target="#step-by-step"><?php echo $text_button_works ?></button>
					<?php else: ?>
						<a href="<?php echo $link_button_works ?>"><button class="mb-4"><?php echo $text_button_works ?></button></a>
					<?php endif;?>
				</div>

			<?php endwhile;
		endif; ?>

	</div>
</div>
