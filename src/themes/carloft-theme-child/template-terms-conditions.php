<?php  
	 /* Template Name: Terms Conditions */
	get_header();
?> 

<section class="terms-conditions">
	<div class="container">
		<?php the_field('content_terms_and_conditions'); ?>
	</div>
</section>

<?php get_footer(); ?>