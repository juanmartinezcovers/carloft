jQuery(document).ready(function($){
    var sliderOurStory = $('.our-donation-slider');
    
    sliderOurStory.slick({
        dots: false,
        infinite: false,
        slidesToShow: 2,
        slidesToScroll: 1,
        centerMode: true,
        centerPadding: '40px',
        responsive: [
            {
                breakpoint: 425,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    centerMode: false,
                    dots: true
                }
            }
        ]
    });
    sliderOurStory.slick("slickGoTo", 1);

    var prevSlick = $('.slick-prev');
    var nextSlick = $('.slick-next');
    var chevronLeft = $('.prev-custom');
    var chevronRight = $('.next-custom');

    chevronLeft.on("click", function(){
        prevSlick.click();
    });
    chevronRight.on("click", function(){
        nextSlick.click();
    });

});