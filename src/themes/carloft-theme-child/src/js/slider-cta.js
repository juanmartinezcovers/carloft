jQuery(document).ready(function($){
    $("#carousel").Cloud9Carousel( {
        buttonLeft: $("#slider-buttons > .left"),
        buttonRight: $("#slider-buttons > .right"),
        autoPlay: 0,
        farScale: 0.4,
        bringToFront: true,
        frontItemClass: 'front',
        onLoaded: function( carouse ){
            let current = document.querySelector('.cloud9-item.front');
            let carType = current.dataset.car_type;
            let button = document.querySelector('[data-car-type]');

            button.innerText = carType;
        },
        onAnimationFinished: function( carousel ){
            // let current = document.querySelector('.cloud9-item.front');
            // let carType = current.dataset.car_type;
            // let button = document.querySelector('[data-car-type]');

            // button.innerText = carType;
        }
    });

    $('.slider').slick({
        dots: true, 
        arrows: false,
        centerMode: true,
        centerPadding: '30px'
    });

    $('.slider').on("afterChange", function(event, slick, currentSlide, nextSlide){
        let current = document.querySelector('.slick-active');
        let carType = current.dataset.car_type;
        let button = document.querySelector('[data-car-type-mobile]');
        let buttonParent = button.parentElement;
        let buttonGrandParent = buttonParent.parentElement;

        buttonGrandParent.href= `/filters-inventory/#${carType}`;
        if (carType == 'suv'){
            buttonParent.innerHTML = `I WANT AN <span data-car-type-mobile="${carType}">${carType}</span>`;
            
        } else {
            buttonParent.innerHTML = `I WANT A <span data-car-type-mobile="${carType}">${carType}</span>`
        }
    })

    $('.how-it-works-slider').slick({
        dots: true, 
        arrows: false,
    });

    $('.swipe-by-category').slick({
        dots: true,
        arrows: false
    });
});