var mainContainer; 
var defaultContent;

$( document ).ready(function() {
   

    mainContainer  = document.getElementById('items');
    defaultContent = document.getElementById('items').innerHTML;
        
    $('form#main-filter').on('change', function(event) {

        let target = event.target;
        let label = target.labels[0]
        
        // If input has data-hidden, we toggle selected class on label
        if (target.type == "checkbox" && target.dataset.hidden){
            label.classList.toggle('selected', target.checked);
        }

        let formData = $('form#main-filter').serializeArray()
        console.log(formData);
        
        if (formData.length > 0){
            let args = {
                formData,
                paged:1, 
                isPagination:false
            }

            filterItems(args);

        } else {
            toggleLoadMore(true);
            mainContainer.innerHTML = defaultContent;
        }
        
    });


   $('#load-more').on('click', function(event) {
        
       let formData = $('form#main-filter').serializeArray();
       let searchData = $('form#search').serializeArray();
       let target = event.target;
       let page = parseInt(target.dataset.paged) + 1;

       let args = {
            formData,
            searchData,
            paged:page, 
            isPagination:true
        }

       filterItems(args);

   });

   $('form#search').on('submit', function(event) {
        event.preventDefault();
        let searchData = $('form#search').serializeArray();
        let formData = $('form#main-filter').serializeArray();

        let searchObj = searchData.find(item => item.name == 's');
        searchData.find(function(item) {
            if(item.name == 'stocknumber'){
                item.value = searchObj.value
            }
        });

        if (searchData.length > 0 ){

            let args = {
                formData, 
                searchData,
                isPagination:false
            }

            filterItems(args);
        }
        
   });
    
});



function itemTemplate(item){

    var  template= `
    <div class="col-md-6 px-md-4 pb-md-5 post-id-${item.id}">
        <figure>
            <a class="" href="https://express.carloft.ca/${item.meta_fields.vin}"><img class="img-fluid inventory-img" src="${item.image}" alt="${item.title}"></a>
        </figure>
        <div class="item px-0 px-md-3">
            <div class="info">
                <p class="name">
                    ${item.title} <span>/ ${item.meta_fields.mileage} km</span>
                </p>
                <p class="price">$${item.meta_fields.bwpayment}</p>
                <p class="payment">Bi-Weekly</p>
            </div>
            <div class="mt-1">
                <a class="primary-button" href="https://express.carloft.ca/${item.meta_fields.vin}">BUILD MY DEAL</a>
            </div>
        </div>
    </div>`

    return template;

}

function filterItems(args) {

    //formData, searchData, paged=1, isPagination=false
    let formData = !args.formData ? [] : args.formData;
    let searchData = !args.searchData ? [] : args.searchData;
    let paged = !args.paged ? 1 : args.paged;
    let isPagination = !args.isPagination ? false : args.isPagination;

    showLoader(true);

    $.ajax({
        type : "post",
        url : ajax_var.url,
        data : {
            action: "filter_inventory", 
            form_data : formData,
            paged:paged,
            s:searchData
        },
        error: function(response){
            console.log(response);
        },
        success: function(response) {
            console.log(response);
            var max_num_pages = parseInt(response.max_num_pages);

            if(response.data){

                showLoader(false);

                var items = '';
                response.data.forEach(element => {
                    items += itemTemplate(element);
                });
                
                if (isPagination){
                    $(mainContainer).append(items);

                    let nextPage = parseInt( $('#load-more').attr('data-paged') )  + 1;
                    $('#load-more').attr('data-paged', nextPage);

                    if (nextPage == parseInt(max_num_pages)){
                        toggleLoadMore(false);
                    } else {
                        toggleLoadMore(true);
                    }

                } else {
                                        
                    if (response.data.length < 1) {
                        mainContainer.innerHTML = '<h4>No vehicles found.</h4>';
                    } else {
                        mainContainer.innerHTML =items;
                    }

                    $('#load-more').attr('data-paged', 1);
                    if (max_num_pages <= 1){
                        toggleLoadMore(false);
                    } else {
                        toggleLoadMore(true);
                    }
                }

                

            }
        }
    });
    
}


function toggleLoadMore(show) {

    if(show){
        $('#load-more').removeClass('d-none');
        $('#load-more').addClass('d-block');
    } else {
        $('#load-more').removeClass('d-block');
        $('#load-more').addClass('d-none');
    }

}

function showLoader(show){
    if(show){
        $('.loader-container').addClass('d-block');
    } else {
        $('.loader-container').removeClass('d-block');
    }
}

window.addEventListener("load",  function() {
    // Run code
    let hash = window.location.hash;
    let truck = $('label[for="category-truck"]');
    let van = $('label[for="category-van"]');
    let sedans = $('label[for="category-car"]');
    let suvs = $('label[for="category-suv"]');


    if (hash === '#truck'){
        truck.trigger("click");
    }else if(hash === '#sedan' ){
        sedans.trigger("click");
    }else if(hash === '#van'){
        van.trigger("click");
    }else if (hash === '#suv'){
        suvs.trigger("click");
        console.log("wepa");
    }
});