 // Initialize and add the map
 function initMap() {
  //Center of the Map 
  var center = new google.maps.LatLng(45.0607914,-74.7708337);
  //Creating map with center location
  var map = new google.maps.Map(
      document.getElementById('map'), {
      zoom: 18, 
      center: center
      });
      
    //Locations Carloft
  var locations = [
         {
        position: new google.maps.LatLng(44.251989,-76.569039),
      type: 'store'
    },{
        position: new google.maps.LatLng(44.254007,-76.957859),
      type: 'store'
    },{
        position: new google.maps.LatLng(45.131165,-76.122748),
      type: 'store'
    },{
        position: new google.maps.LatLng(45.0606713, -74.7704054),
      type: 'store'
    }
  ];
 
  //Creating custom markers
  for( var i = 0; i < locations.length; i++){
      var markers = new google.maps.Marker({
        position: locations[i].position,
      map: map
    });
  }
  
}