jQuery(document).ready(function($){

    var msgInput = $('#message')
    if ( msgInput.length > 0 ){
        msgInput.rows = 5;
    }

    function validateEmail(email) {
        const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }

    function validate(){
        var mail = $('[type="email"]').val();
        var submit = $('.wpcf7-submit');
        if ( validateEmail(mail) ){
            submit.attr("disabled", false);
        } else {
            submit.attr("disabled", true);
        }
    }

    $('[type="email"]').keyup(validate);
});