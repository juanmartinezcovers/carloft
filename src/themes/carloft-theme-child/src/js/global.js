jQuery(document).ready(function($){
  
  function animationBtnSearch() {
    const submit = $('.submit');
    const topInput = $('.input-field');

    topInput.keyup(function(){
      if ( !$(this).val() ){
        submit.fadeOut();
      } else {
        submit.removeClass('d-none');
        submit.fadeIn();
      }
    });
  }

  if ($(window).width() > 700) {
    animationBtnSearch();
  }

  const sliderTextHome = $('.big-text-slider');
  sliderTextHome.slick({
    autoplay: true,
    infinite: true,
    arrows: false,
    dots: false,
    slidesToShow: 1,
    slidesToScroll: 1
  });

  const searchIcon = $('.search-toggle');
  const searchContainer = $('.search-mobile')
  searchIcon.on("click", function(){
    searchContainer.toggleClass('d-none');
  });
  const toggleMenuMobile = $('[data-toggle="mobile-menu"]');
  const menuMobileContainer = $('.fixed-mobile');
  toggleMenuMobile.on("click", function(){
    if( menuMobileContainer.hasClass('opened-menu') ){
      menuMobileContainer.removeClass('opened-menu');
    }else {
      menuMobileContainer.addClass('opened-menu');
    }
  })

  //Footer
var container = $('.second-column .contact-info');
var p1 = $('.contact-4');
var p2 = $('.contact-5');
var p3 = $('.contact-6');
var p4 = $('.contact-7');

p1.appendTo(container);
p2.appendTo(container);
p3.appendTo(container);
p4.appendTo(container);

//Menu
let sellLink = $('.menu-item-839 a');
let approvedLink = $('.menu-item-838 a')
let content = $('body');

if (!content.hasClass('home')){
    sellLink.each(function(index){
       $(this).attr('href', '/home/#we-pay'); 
    });
    approvedLink.each(function(index){
        $(this).attr('href', '/home/#pre-approved');
    });
    console.log("is not home")
} 

let sellCarLink = $('.sell-a-car');

sellCarLink.on("click", function(event){
  event.preventDefault();

  $('#step-by-step').modal("show");
});

});