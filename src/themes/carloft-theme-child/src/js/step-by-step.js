jQuery(document).ready(function($){

    const wrapperContainer = $('.form-wrapper');
    const submit = $("[data-form='submit']");
    const tab = $('[data-form="tab"]');

    var formStepsInputs = {
        make: '',
        model: '',
        year: '',
        message: '',
        seller: '',
        phone: '',
        email: ''
    }


    const step = $('div[data-form="tab"]');

    function validateEmail(email) {
        const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
      }
    step.each(function(index)
    {
        let mail = $(this).find('input[type="email"]');
        let submit =  $(this).find('[data-form="submit"]');
        let input = $(this).find("input");
        let message = $(this).find("#message-step");
        submit.attr("disabled", true);
        let back_button=$(this).find('[data-form="back"]');
        if(back_button.length)
        {
            back_button.on("click", function()
            {
                step[index].classList.add("d-none");
                step[index-1].classList.remove("d-none");
            });
        }
        function inputMail(){
            if (validateEmail(mail)){
                submit.attr("disabled", false);
            }
        }

        function inputCheck(){
            input.each(function(index){
                if(input.val()){
                    submit.attr("disabled", false);
                } 
            })
        }
       
        mail.keyup(inputMail);
        input.keyup(inputCheck);
        message.keyup(inputCheck);
    });

    submit.each(function(index)
    {
        $(this).on("click", function()
        {
            switch(index){
                case (0):
                    formStepsInputs.make = DOMPurify.sanitize($('#make').val());
                    formStepsInputs.model= $('#model').val();
                    formStepsInputs.year = $('#year').val();
                    localStorage.setItem('data-steps-form', JSON.stringify(formStepsInputs));
                    break;
                case(1):
                    formStepsInputs.message = $('#message-step').val();
                    localStorage.setItem('data-steps-form', JSON.stringify(formStepsInputs));
                    break;
                case(2):
                    let styles = {
                        "color": "white",
                        "opacity": "1" 
                    }

                    let close = $('.close');
                    close.css(styles);
                    formStepsInputs.seller = $('#seller').val();
                    formStepsInputs.phone = $('#phone-step').val();
                    formStepsInputs.email = $('#email-step').val();
                    formStepsInputs.km = $('#km').val();
                    localStorage.setItem('data-steps-form', JSON.stringify(formStepsInputs));
                    var json = JSON.parse(localStorage.getItem("data-steps-form"));
                    $.ajax({
                        type : "post",
                        url: ajax_var.url,
                        data : {
                            action: ajax_var.action, 
                            form : json,
                        },
                        error: function(response){
                            console.log(response);
                        },
                        success: function(response) {
                            // Actualiza el mensaje con la respuesta
                            setTimeout(function(){
                                $('#step-by-step').modal("hide");
                                window.location.href = "/home";
                            }, 1000);
                        }
                    })
                    break;
            }
            
            tab[index].classList.add("d-none");
            var nextTab = index + 1;
            var end = step[nextTab];
            if( typeof end != "undefined" )
            {
                step[nextTab].classList.remove("d-none");
            }
            else
            {
                $("[data-modal='step-form']").addClass("d-none");
                $("[data-form='last-step']").removeClass("d-none");
            }

          
        }); 
    });

});