jQuery(document).ready(function($){ 

    $.fn.serializeObject = function(){
        var o = {};
        var a = this.serializeArray();
        $.each(a, function() {
            if (o[this.name]) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };

    var next = document.querySelector(".next");
    var prev = document.querySelector(".prev");
    var tabs = document.querySelectorAll(".tab");
    var progress = document.querySelector(".progress");
    var percentage = 100 / tabs.length;
    var container = document.querySelector('.tabs-container');
    var congratsModal = $("#congrats");
    var form = document.getElementById("step-form-page");
    var formObject = $("#step-form-page")
    var formCongrats = document.getElementById('congrats-form');
    var pageTitle = document.querySelector('[data-step-title]');

    var hide = function(elem) {
        elem.style.display = 'none';
    };

    var show = function(elem){
    elem.style.display = 'block';
    }

    function generateSubmit(){
        var controlsWrapper = document.querySelector('.controls');
        var inputSubmit = document.createElement('button');
        inputSubmit.setAttribute('type', 'submit');
        inputSubmit.setAttribute('id', 'form-submit-steps');
        inputSubmit.setAttribute('form', 'step-form-page');
        inputSubmit.setAttribute('class', 'mb-3 mb-md-0');
        inputSubmit.innerText = 'Continue';
        hide(next);
        controlsWrapper.appendChild(inputSubmit);
    }

    function progressBar(step){
        progress.style.width = `${percentage * step}%`;
    } 

    var prevStep = function(elem){
        let target = elem.target;
        let prevStep = parseInt(target.dataset.prev) - 1;


        var pageStep = parseInt(pageTitle.dataset.stepTitle);
        var title = pageStep - 1;
        pageTitle.dataset.stepTitle = title;
        pageTitle.innerHTML = title;
        //Back to prev tab
        if(target.dataset.prev >= 1){
            next.dataset.step = parseInt(target.dataset.prev);
            target.dataset.prev = prevStep;
            container.style.transform = `translatex(-${prevStep * 100}%)`;
            progressBar(prevStep);
            
        }

        if (target.dataset.prev < 1){
            hide(target);
        }
        
    
    }

    var nextStep = function(elem){
        let target = elem.target;
        var currentStep = parseInt(target.dataset.step);
        var pageStep = parseInt(pageTitle.dataset.stepTitle);
        var title = pageStep + 1;
        pageTitle.dataset.stepTitle = title;
        pageTitle.innerHTML = title;
        if (target.dataset.step < tabs.length){
            target.dataset.step = currentStep + 1;
            prev.dataset.prev = currentStep;
            //Next Tab
            container.style.transform = `translatex(-${currentStep * 100}%)`;
            progressBar(currentStep);
            if (prev.dataset.prev >= 1){
                show(prev);
            }
        }

        if (target.dataset.step == tabs.length){
            progressBar(currentStep);
            generateSubmit();            
        }
    }
    prev.addEventListener("click", prevStep);
    next.addEventListener("click", nextStep );

    form.addEventListener('submit', function(e){
        var stepForm = formObject.serializeObject();
        localStorage.setItem('step-page', JSON.stringify(stepForm));
        e.preventDefault();
        var data = JSON.parse(localStorage.getItem('step-page'));
       console.log(data);
       var locationModal = document.getElementById('location');
       locationModal.value = data.stepLocation;
       congratsModal.modal("show");
       
    });

    formCongrats.addEventListener('submit', function(e){
        e.preventDefault();
        var data = $("#congrats-form").serializeObject();
        localStorage.setItem('congrats-form', JSON.stringify(data));
        var step_json = JSON.parse(localStorage.getItem('step-page'));
        var json = JSON.parse(localStorage.getItem('congrats-form'));
        $.ajax({
            type : "post",
            url: ajax_var.url,
            data : {
                action: ajax_var.action, 
                form : json,
                steps: step_json
            },
            error: function(response){
                console.log(response);
            },
            success: function(response) {
                // Actualiza el mensaje con la respuesta
                console.log(response);
                setTimeout(function(){
                    $('#congrats').modal("hide");
                    window.location.href = "/home";
                }, 1000);
            }
        })
        
    });

    console.log(ajax_var.url, ajax_var.action);
});

