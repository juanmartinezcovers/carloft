<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since Twenty Nineteen 1.0
 */
?>

<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<link rel="profile" href="https://gmpg.org/xfn/11" />
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<div class="loader-container">
  <div class="loader"></div>
</div>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php _e( 'Skip to content', 'twentynineteen' ); ?></a>

  <?php  get_template_part( 'template-parts/header/top', 'bar' ); 
         get_template_part( 'template-parts/header/header', 'menu' );
  ?>

	
	<div id="content" class="site-content">
<section id="section-header-home" class="top">
  <div class="container-fluid container-padding">
    <div class="row pt-6 align-items-center container-header-title">

      <div class="col-md-6">
        <div class="cta-home">
         
          <?php 
          global $post;

          if (!is_404())
            $slug = $post->post_name;

          if ( is_home() ){
            get_template_part('template-parts/header/title', 'home');
          } elseif ( 'contact-form' == get_post_type() )  {
            get_template_part( 'template-parts/header/title', 'contact' );
          } elseif( is_404() ) {
            get_template_part('template-parts/header/title', '404');
          }else {
            get_template_part('template-parts/header/title', $slug );
          }
        ?>
        </div>
       
       <?php if( is_home() || is_front_page() ): ?>
        <div class="cta-button">
        <?php $url = get_field('main_banner_link_button' , 'option'); ?>
          <a href="<?php echo $url["url"]; ?>" class="btn-cta"><?php the_field('main_banner_text_button' , 'option'); ?></a>
        </div>
       <?php endif; ?>
      </div>
      
      <div class="col-md-6 d-none d-md-flex">
			 <div class="hero-letters">
          <?php the_field('main_banner_content_text', 'option'); ?>
			 </div>
      </div>

    </div>
  </div>
</section>